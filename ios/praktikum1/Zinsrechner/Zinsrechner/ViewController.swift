//
//  ViewController.swift
//  Zinsrechner
//
//  Created by Oliver Erxleben on 15.04.15.
//  Copyright (c) 2015 HS Osnabrueck. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var interestsModel = Interests()
    
    @IBOutlet weak var amount: UITextField!
    @IBOutlet weak var duration: UITextField!
    @IBOutlet weak var interest: UITextField!
    @IBOutlet weak var accumulate_yes: UISegmentedControl!
    @IBOutlet weak var display: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func calculate(sender: AnyObject) {
        
        interestsModel.amount = Double(amount.text.toInt()!)
        interestsModel.duration = duration.text.toInt()!
        interestsModel.interest = Double(interest.text.toInt()!)
        let myselect = accumulate_yes.selectedSegmentIndex
        
        var result = interestsModel.calculate(InterestType(rawValue: accumulate_yes.selectedSegmentIndex)!)
        
//        for index in 1 ... myduration {
//            if myselect == 0{
//                result += result * myinterest / 100
//            } else {
//                result += myamount * myinterest / 100 
//            }
//        }
        
        var displayText = String(format: "%.02f", result!)
        display.text = "\(displayText) €"
    }
    
    

}

