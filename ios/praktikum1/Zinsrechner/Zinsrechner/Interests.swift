//
//  Interests.swift
//  Zinsrechner
//
//  Created by Oliver Erxleben on 15.04.15.
//  Copyright (c) 2015 HS Osnabrueck. All rights reserved.
//

import Foundation

enum InterestType: Int {
    case Disburse = 0 // Zinsauszahlung
    case Accumulate = 1 // Zinssammlung
}

class Interests {
    var amount: Double = 0.0
    var duration: Int = 0
    var interest: Double = 0.0
    
    func calculate(type: InterestType) -> Double? {
        var result: Double = amount
        
        for index in 1...duration {
            switch type {
            case .Disburse:
                result += result * interest / 100
            case .Accumulate:
                result += amount * interest / 100
            }
        }
        
        return result
    }
}