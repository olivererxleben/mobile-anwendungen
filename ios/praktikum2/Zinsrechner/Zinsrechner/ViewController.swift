//
//  ViewController.swift
//  Zinsrechner
//
//  Created by Oliver Erxleben on 15.04.15.
//  Copyright (c) 2015 HS Osnabrueck. All rights reserved.
//

import UIKit

class ViewController: UIViewController, InterestLoaderDelegate {

    var interestsModel = Interests()
    
    @IBOutlet weak var amount: UITextField!
    @IBOutlet weak var duration: UITextField!
    @IBOutlet weak var durationSlider: UISlider!
   
    @IBOutlet weak var interest: UITextField!
    
    @IBOutlet weak var accumulate_yes: UISegmentedControl!
    @IBOutlet weak var display: UILabel!

    var actInd : UIActivityIndicatorView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        interestsModel.loadDelegate = self
        duration.text = "\( Int(durationSlider.value))"
        
        // configure Activity Indicator
         actInd = UIActivityIndicatorView(frame: CGRectMake(0,0, 50, 50)) as UIActivityIndicatorView
        actInd.center = self.view.center
        actInd.hidesWhenStopped = true
        actInd.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
        view.addSubview(actInd)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadingFinished(interest: Double) {
        
        println(interest.description)
        self.interest.text = interest.description
        
        actInd.stopAnimating()
        
    }

    @IBAction func calculate(sender: AnyObject) {
        println(self.interest.text);
        
        interestsModel.amount = Double(amount.text.toInt()!)
        interestsModel.duration = duration.text.toInt()!
        
        interestsModel.interest = (interest.text! as NSString).doubleValue
        
        let myselect = accumulate_yes.selectedSegmentIndex
        
        var result = interestsModel.calculate(InterestType(rawValue: accumulate_yes.selectedSegmentIndex)!)
        
        var displayText = String(format: "%.02f", result!)
        display.text = "\(displayText) €"
    }
    
    @IBAction func loadXML(sender: UIButton) {
       
        actInd.startAnimating()
        
        interestsModel.loadInterest( NSURL( string: "http://webservices.lb.lt/LBInterestRates/LBInterestRates.asmx/getLBInterestRatesByDate?Date=2014-03-20" )! )
    }
    
    @IBAction func durationInputDidEndEditing(sender: UITextField) {
        
        var current = sender.text
        
        durationSlider.value = (current as NSString).floatValue
        
    }
    
    @IBAction func sliderValueChanged(sender: UISlider) {
        var current = Int(sender.value)
        
        duration.text = "\(current)"
    }
}

