//
//  Interests.swift
//  Zinsrechner
//
//  Created by Oliver Erxleben on 15.04.15.
//  Copyright (c) 2015 HS Osnabrueck. All rights reserved.
//

import Foundation

enum InterestType: Int {
    case Disburse = 0 // Zinsauszahlung
    case Accumulate = 1 // Zinssammlung
}

protocol InterestLoaderDelegate {
    
    func loadingFinished( interest: Double )
}

class Interests: NSObject, NSXMLParserDelegate {
    var amount: Double? = nil
    var duration: Int? = nil
    var interest: Double? = nil
    
    var loadDelegate: InterestLoaderDelegate? = nil
    
    private var currentElement = ""
    private var foundCharacters = ""
    private let queue = NSOperationQueue()
    
    func calculate(type: InterestType) -> Double? {
        if( amount == nil || duration == nil || interest == nil ){
            return nil
        }
        
        var result: Double = amount! // start value
        
        for index in 1...duration! {
            switch type {
            case .Disburse:
                println()
                result += result * interest! / 100
            case .Accumulate:
                result += amount! * interest! / 100
            }
        }
        
        return result
    }
    
    func loadInterest( url: NSURL ){
        if queue.operationCount < 1 {
            queue.addOperationWithBlock(){
                //load raw xml async
                let parser = NSXMLParser(contentsOfURL: url)
                self.currentElement = ""
                self.foundCharacters = ""
                parser!.delegate = self
                parser!.parse()
            }
        }
        else {
            println("Already fetching!")
        }
    }

    func parser(parser: NSXMLParser,
        didStartElement elementName: String,
        namespaceURI: String?,
        qualifiedName qName: String?,
        attributes attributeDict: [NSObject : AnyObject]) {
            println(elementName)
            currentElement = elementName
    }
    
    func parser(parser: NSXMLParser, foundCharacters string: String?) {
        if currentElement == "rate" && string!.rangeOfString("\n") == nil {
            foundCharacters += string!
        }
    }
    
    func parserDidEndDocument(parser: NSXMLParser) {
        //after parse delagate result on main thread
        NSOperationQueue.mainQueue().addOperationWithBlock() {
            if self.loadDelegate != nil {
                let interestValue = NSNumberFormatter().numberFromString(self.foundCharacters)!.doubleValue;
                self.loadDelegate?.loadingFinished(interestValue)
            }
        }
    }
}

