# Example Application for Cordova / Sencha Touch

## Prerequisites

The example application utilizes **npm** (cordova specific build tools) and **bower** (frontend specific libs and tools). On the development machine, both must be installed. 

## Setup after checkout

cordova specific hooks are managed by npm, so it will not be versioned. The same goes for bower components and Cordova Plugins. In the end it cordova needs to build the package. 

		npm install && bower install && cordova build		

