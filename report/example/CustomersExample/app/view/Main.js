Ext.define('CustomersExample.view.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'main',
    requires: [
        'Ext.TitleBar',
        'Ext.Video',
				'Ext.List'
    ],
    config: {
        tabBarPosition: 'bottom',

        items: [
            {
                title: 'Kunden',
                iconCls: 'team', // team.png default picto

                styleHtmlContent: true,
                scrollable: true,

                items: {
                    docked: 'top',
                    xtype: 'titlebar',
                    title: 'Customers Example'
                },

                html: [
                    "You've just generated a new Sencha Touch 2 project. What you're looking at right now is the ",
                    "contents of <a target='_blank' href=\"app/view/Main.js\">app/view/Main.js</a> - edit that file ",
                    "and refresh to change what's rendered here."
                ].join("")
            },
            {
                title: 'Information',
                iconCls: 'info',

                items: [
                    {
                        docked: 'top',
                        xtype: 'titlebar',
                        title: 'Getting Started'
                    },

										{
  									  xtype: 'panel',
									    html: '<img style="height: 100px; width: 100px;" src="http://addressofmyimage.com/image.png" />'
											
										}
                    // {
                    //     xtype: 'video',
                    //     url: 'http://av.vimeo.com/64284/137/87347327.mp4?token=1330978144_f9b698fea38cd408d52a2393240c896c',
                    //     posterUrl: 'http://b.vimeocdn.com/ts/261/062/261062119_640.jpg'
                    // }
                ]
            }
        ]
    },

		initialize: function() {
			this.callParent(arguments);

			var list = Ext.create('Ext.List', {
   		 	fullscreen: false,
				title: 'yay',
    		itemTpl: '{title}',
		    data: [
    	    { title: 'Item 1' },
      	  { title: 'Item 2' },
        	{ title: 'Item 3' },
        	{ title: 'Item 4' }
    		]
			});

			this.add(list);
		}
});
