Mobile Anwendungen
-------------------

Semesterprojektdateien.

Wiki
----

Das Projekt verfügt über ein Wiki, welches ebenfalls ein Git-Repository ist: https://bitbucket.org/olivererxleben/mobile-anwendungen/wiki/Home, bzw.

    git clone https://olivererxleben@bitbucket.org/olivererxleben/mobile-anwendungen.git/wiki

Gedacht ist es mit Hilfe des Wikis ein Informationsmanagement aufzubauen um die Seminar- und Projektarbeit umzusetzen.

Ticket-System
-------------

Das Ticketsystem kann als Bugtracker und TODO Manager betrachtet werden.

Projektideen
---------------

Finden sich hier: https://bitbucket.org/olivererxleben/mobile-anwendungen/wiki/Projektarbeit:%20Ideen