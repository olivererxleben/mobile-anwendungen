//
//  HyKMessageController.swift
//  HybridKit
//
//  Created by Oliver Erxleben on 28.07.15.
//  Copyright © 2015 Hochschule Osnabrück. All rights reserved.
//

import UIKit
import WebKit

/** 
    Protocol desribes fields for HyKMessages
*/
protocol HyKMessageProtocol {
    var identifier: String {get set}
    var type: HyKMessageType {get set}
    var handler: (params: AnyObject) -> AnyObject {get}
    
    var onSuccess: String? {get set}
    var onFailure: String? {get set}
    var onProgress: String? {get set}
}

/**
    Initalizes a HyKMessage conforming to the HyKMessageProtocol

    - parameter identifier: the Name of the Message
    - parameter type: Type of ACTION, TASK, EXT (HykMessageType)
    - parameter handler: a function to be passed to which will be called from webView
*/
public class HyKMessage: HyKMessageProtocol {
    var identifier: String
    var type: HyKMessageType
    var handler: (params: AnyObject) -> AnyObject
    var onSuccess: String? = ""
    var onFailure: String? = ""
    var onProgress: String? = ""
    
    public init(identifier: String, type: HyKMessageType, handler: (params: AnyObject) -> AnyObject ) {
        self.identifier = identifier
        self.type = type
        self.handler = handler
        
    }
}