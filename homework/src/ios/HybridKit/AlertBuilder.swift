//
//  AlertBuilder.swift
//  HybridKit
//
//  Created by Oliver Erxleben on 07.08.15.
//  Copyright © 2015 Hochschule Osnabrück. All rights reserved.
//

import UIKit

public class AlertBuilder: NSObject {
    var alertController: UIAlertController
    
    init(title: String, message: String, style: UIAlertControllerStyle) {
        self.alertController = UIAlertController(title: title, message: message, preferredStyle: style)
    }
    
    func setPopoverPresentationProperties(sourceView: UIView? = nil, sourceRect:CGRect? = nil, barButtonItem: UIBarButtonItem? = nil, permittedArrowDirections: UIPopoverArrowDirection? = nil) -> Self {
        
        if let poc = alertController.popoverPresentationController {
            if let view = sourceView {
                poc.sourceView = view
            }
            if let rect = sourceRect {
                poc.sourceRect = rect
            }
            if let item = barButtonItem {
                poc.barButtonItem = item
            }
            if let directions = permittedArrowDirections {
                poc.permittedArrowDirections = directions
            }
        }
        
        return self
    }
    
    public func addAction(title: String = "", style: UIAlertActionStyle = .Default, handler: ((UIAlertAction!) -> Void) = { _ in }) -> Self {
        alertController.addAction(UIAlertAction(title: title, style: style, handler: handler))
        return self
    }
    
    public func addTextFieldHandler(handler: ((UITextField!) -> Void) = { _ in }) -> Self {
        alertController.addTextFieldWithConfigurationHandler(handler)
        return self
    }
    
    public func build() -> UIAlertController {
        return alertController
    }
}

public extension UIAlertController {
    public func alertShow(animated: Bool = true, completionHandler: (() -> Void)? = nil) {
        if let rootVC = UIApplication.sharedApplication().keyWindow?.rootViewController {
            var forefrontVC = rootVC
            while let presentedVC = forefrontVC.presentedViewController {
                forefrontVC = presentedVC
            }
            forefrontVC.presentViewController(self, animated: animated, completion: completionHandler)
        }
    }
}