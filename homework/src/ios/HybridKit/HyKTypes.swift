//
//  Types.swift
//  HybridKit
//
//  Created by Oliver Erxleben on 08.08.15.
//  Copyright © 2015 Hochschule Osnabrück. All rights reserved.
//

import Foundation

/**
HyKWVLoadTypes defines Type for local or remote loading

- LOCAL: Local url
- REMOTE: Remote url
*/
public enum HyKWebViewLoadType {
    case LOCAL
    case REMOTE
}

/**
HyKMessageType defines types of Messages
- ACTION: a simple call-return cycle
- TASK: a process with callbacks and completionHandling
- EXTENSION: a complex function and object mapping 
*/
public enum HyKMessageType {
    case ACTION
    case TASK
    case EXTENSION
}

/**
HyKMessageCallbackType defines the type of message
- SUCCESS: uses the specified onSuccess callback
- FAIL: uses the specified onFailure callback
- PROGRESS: uses the specified onProgress callback
*/
public enum HyKMessageCallbackType {
    case SUCCESS
    case FAIL
    case PROGRESS
}