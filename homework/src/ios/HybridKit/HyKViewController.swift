//
//  HyKViewController.swift
//  HybridKit
//
//  Created by Oliver Erxleben on 07.08.15.
//  Copyright © 2015 Hochschule Osnabrück. All rights reserved.
//

import UIKit
import WebKit

public class HyKViewController: UIViewController {

    var webView: WKWebView!
    
    var contentController: WKUserContentController = WKUserContentController()
    var handlers: NSMutableDictionary = NSMutableDictionary()
    var userScripts: NSMutableDictionary = NSMutableDictionary()
    var progressView: UIProgressView?
   
    // MARK: - Life Cycle
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        //configure() should be called here
    }
    
    override public func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override public func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        self.view.addSubview(webView)
    }

    public override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
 
        
    // MARK: - HyK Conf
    /**
        Configures the webview according configuration.
        - parameter configuration: (HyKConfiguration) the config struct
        - returns: Void
    */
    public func configure(configuration: HyKConfiguration? = nil) {
        
        let conf: HyKConfiguration
        
        if (configuration != nil) {
            conf = configuration!
        }
        else {
           conf = HyKConfiguration()
        }
        
        // messages
        if (conf.messages != nil) {
            
            // defaults
            initDefaultUserScript()
            initDefaultMessageHandler()
            
            // custom messages
            for message in conf.messages! {
                let m = message.value as! HyKMessage
                
                handlers[m.identifier] = m
                
            }
            
            // configure webVIew userContentController
            addUserScripts()
            addMessageHandlers()
            conf.webViewConfiguration!.userContentController = contentController
        }
        
        webView = WKWebView(frame: conf.frame!, configuration: conf.webViewConfiguration!)
        
        // set delegates
        webView.navigationDelegate = self
        webView.UIDelegate = self
        
        // load web content
        if (conf.loadType == HyKWebViewLoadType.REMOTE) {
            let url = NSURL(string: conf.url)
            let req = NSURLRequest(URL: url!)
            
            webView.loadRequest(req)
            
        }
        
        if (conf.loadType == HyKWebViewLoadType.LOCAL) {
            // set local file URL loading
            let url = NSURL(string: conf.url)
            
            if #available(iOS 9.0, *) {
                webView.loadFileURL(url!, allowingReadAccessToURL: url!)
            } else {
                // Fallback on earlier versions
                var filePath = NSBundle.mainBundle().pathForResource("index", ofType: "html")
                filePath = pathForBuggyWKWebView(filePath)
                let req = NSURLRequest(URL: NSURL.fileURLWithPath(filePath!))
                webView.loadRequest(req)
            }
            
        }
        
        // Observers
        webView.addObserver(self, forKeyPath: "estimatedProgress", options: NSKeyValueObservingOptions.New, context: nil)
    }
    
    // MARK: - HybridKit Callback
    /**
        implements the callback of hyk.init() and connects Webkit callbacks to HyKMessage
    
        - parameter params: AnyObject
        
        - returns: AnyObject
    */
    private func callbackImplementation (params: AnyObject) -> AnyObject {

        do {
            let jsonString = params as! String
            let json = jsonString.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)
            let jsonArray : NSArray = try NSJSONSerialization.JSONObjectWithData(json!, options: NSJSONReadingOptions.MutableContainers) as! NSArray
            
           
           
            for (index, message) in jsonArray.enumerate() {
            
                let jsonDict = jsonArray.objectAtIndex(index)
                
                let name = jsonDict.objectForKey("name") as! String
                if let onSuc = jsonDict.objectForKey("onSuccess")  {
                     (handlers.objectForKey(name) as! HyKMessage).onSuccess = onSuc as? String
                }
                
                if let onFail = jsonDict.objectForKey("onFailure") {
                    (handlers.objectForKey(name) as! HyKMessage).onFailure = onFail as? String
                }
                
                if let onProg = jsonDict.objectForKey("onProgress") {
                    
                    print(onProg)
                    (handlers.objectForKey(name) as! HyKMessage).onProgress = onProg as? String
                }
                
            }
            
        } catch let error as NSError {
            print(error)
        }
        
        //webView.evaluateJavaScript("alert('yay');", completionHandler: nil)
        
        
        return 0
    }
    
    
    // MARK: - ContentController
    /** 
        Sends an async (Callback-)Message to the WebView
        - parameter name: Name of the Message
        - parameter type: type of SUCCESS, FAIL, PROGRESS (HyKMessageCallbackType)
        - parameter data: A String of data to send
    
        - returns: Void
    */
    public func sendMessage(name: String, type: HyKMessageCallbackType, data: String) {
        
        let message = (handlers[name] as! HyKMessage)
        let scriptName: String
        
        // check type of callback
        switch type {
            
        case .SUCCESS:
            scriptName = message.onSuccess!
            break;
            
        case .FAIL:
            scriptName = message.onFailure!
            break;
            
        case .PROGRESS:
            scriptName = message.onProgress!
            break;
        }
        
        let script = "\(scriptName)('\(data)');"
        webView.evaluateJavaScript(script, completionHandler: nil)
    }

    
    private func initDefaultUserScript() {
        // set default init user script
        let hykInit = initScript()
        userScripts["HyKinit"] = hykInit
    }
    
    private func initDefaultMessageHandler() {
        // set default init callback message handler
        let initCallback = HyKMessage(identifier: "initCallback", type: HyKMessageType.ACTION, handler: callbackImplementation)
        handlers["initCallback"] = initCallback
        
    }
    
    private func initScript() -> WKUserScript {
        
        return WKUserScript(source: "hyk.init();", injectionTime: WKUserScriptInjectionTime.AtDocumentEnd, forMainFrameOnly: true)
    }

    private func addUserScripts () {
        // User Scripts
        for script in userScripts {
            contentController.addUserScript(script.value as! WKUserScript)
            
        }
    }
    
    private func addMessageHandlers() {
        // message handlers
        for handler in handlers {
            let m = handler.value as! HyKMessage
            
            contentController.addScriptMessageHandler(self, name: m.identifier)
            
        }
    }
    
    func pathForBuggyWKWebView(filePath: String?) -> String? {
        let fileMgr = NSFileManager.defaultManager()
        let tmpPath = NSTemporaryDirectory().stringByAppendingPathComponent("www")
       
        do {
            try fileMgr.createDirectoryAtPath(tmpPath, withIntermediateDirectories: true, attributes: nil)
            
        } catch {
            print("Couldn't create www subdirectory.")
        }
      
        let dstPath = tmpPath.stringByAppendingPathComponent(filePath!.lastPathComponent)
        if !fileMgr.fileExistsAtPath(dstPath) {
            do {
                try fileMgr.copyItemAtPath(filePath!, toPath: dstPath)
                
            } catch _ {
                 print("Couldn't copy file to /tmp/www.")
            }
            
        }
        return dstPath
    }

}