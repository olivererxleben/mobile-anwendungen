//
//  HybridKit.h
//  HybridKit
//
//  Created by Oliver Erxleben on 22.07.15.
//  Copyright © 2015 Hochschule Osnabrück. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for HybridKit.
FOUNDATION_EXPORT double HybridKitVersionNumber;

//! Project version string for HybridKit.
FOUNDATION_EXPORT const unsigned char HybridKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <HybridKit/PublicHeader.h>


