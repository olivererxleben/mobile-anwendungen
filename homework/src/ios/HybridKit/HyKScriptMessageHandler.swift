import WebKit

/** 
 Extension implements The ScriptMessageHandler of WKScriptMessageHandler protocol
*/
extension HyKViewController: WKScriptMessageHandler {
    
    /**
    userContenController didReceiveMessage looks up handlers dictionary for function an calls the registered handler function
    */
    public func userContentController(userContentController: WKUserContentController, didReceiveScriptMessage message: WKScriptMessage) {
        
        for handler in handlers {
            
            if handler.key as! String == message.name {
                let m = handler.value as! HyKMessage
                m.handler(params: message.body)
                break
            }
        }
    }
    
}