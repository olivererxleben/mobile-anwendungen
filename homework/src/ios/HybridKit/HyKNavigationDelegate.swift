import WebKit

/** 
Extension of WebKit`s NavigationDelegate protocol
*/
extension HyKViewController: WKNavigationDelegate {
    
    /**
    called when navigation (page loading) started
    */
    public func webView(webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        
        progressView = UIProgressView(progressViewStyle: UIProgressViewStyle.Default)
        progressView!.center = self.view.center
        
        self.view.addSubview(progressView!)
    }
    
    /**
    called when navigating (page loading) completed
    */
    public func webView(webView: WKWebView, didFinishNavigation navigation: WKNavigation!) {
        
        progressView?.removeFromSuperview()
        
    }
    
    /**
    observes the estimated progress of navigation and updates the progress view
    */
    override public func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>) {
        if keyPath == "estimatedProgress" {
            print(webView.estimatedProgress)
            progressView?.progress = Float(webView.estimatedProgress)
        }
    }
    
}