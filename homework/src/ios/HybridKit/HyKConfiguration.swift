//
//  HyKWebViewConfiguration.swift
//  HybridKit
//
//  Created by Oliver Erxleben on 21.07.15.
//  Copyright © 2015 hsos. All rights reserved.
//

import Foundation
import WebKit

/**
    HybridKit WebView Configuration
    
    - type: Type of Webview (LOCAL | REMOTE)
    - url: the URL to load
    - request: The URL Request Object 
    - webViewConfiguration: Web Configuration Object (WKWebViewConfiguration), defaults to HyKNavigationDelegate()
    - viewFrame:
    - messages: 

*/
public struct HyKConfiguration {
    // required
    var loadType: HyKWebViewLoadType
    var url: String
    
    // optional
    var webViewConfiguration: WKWebViewConfiguration? = WKWebViewConfiguration()
    var frame: CGRect? = UIScreen.mainScreen().applicationFrame
    var messages: NSMutableDictionary? = nil
   
    
    public init() {
        loadType = HyKWebViewLoadType.REMOTE
        url  = "https://www.google.com"
    }
    
    public init(url: String, loadType: HyKWebViewLoadType, configuration: WKWebViewConfiguration? = nil,
        frame: CGRect? = nil,
        messages: NSMutableDictionary? = nil,
        debug: Bool? = nil)
    {
            
            self.url = url
            self.loadType = loadType
            
            if (configuration != nil) {
                webViewConfiguration? = configuration!
            }
            
            if (frame != nil) {
                self.frame = frame
            }
            
            if (messages != nil) {
                self.messages = messages
            }
    }
}