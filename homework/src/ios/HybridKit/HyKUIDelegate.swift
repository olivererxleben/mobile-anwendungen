import WebKit

/**
Extension of WebKit`s UIDelegate protocol
*/
extension HyKViewController: WKUIDelegate {
    
    /**
    called when webView throws an alert
    */
    public func webView(webView: WKWebView, runJavaScriptAlertPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: () -> Void) {
        
        let alertCtrl = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        let action = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil)
        
        alertCtrl.addAction(action)
        self.presentViewController(alertCtrl, animated: true, completion: completionHandler)
        
    }
    
    /**
    called when webView throws a confirm
    */
    public func webView(webView: WKWebView, runJavaScriptConfirmPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: (Bool) -> Void) {
        
        let alertCtrl = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.ActionSheet)
        alertCtrl.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: { action in
            completionHandler(false)
        }))
        alertCtrl.addAction(UIAlertAction(title: "OK", style: .Default, handler: { action in
            completionHandler(true)
        }))
        self.presentViewController(alertCtrl, animated: true, completion: nil)
        
    }
    
    /** 
    called when webView throws a prompt
    */
    public func webView(webView: WKWebView, runJavaScriptTextInputPanelWithPrompt prompt: String, defaultText: String?, initiatedByFrame frame: WKFrameInfo, completionHandler: (String?) -> Void) {
        let alertCtrl = UIAlertController(title: frame.request.URL?.host, message: prompt, preferredStyle: .Alert)
        weak var alertTextField: UITextField!
        alertCtrl.addTextFieldWithConfigurationHandler { textField in
            textField.text = defaultText
            alertTextField = textField
        }
        alertCtrl.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: { action in
            completionHandler(nil)
        }))
        alertCtrl.addAction(UIAlertAction(title: "OK", style: .Default, handler: { action in
            completionHandler(alertTextField.text)
        }))
        self.presentViewController(alertCtrl, animated: true, completion: nil)
    }
    

    
}