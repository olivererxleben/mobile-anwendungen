//
//  ViewController.swift
//  SinglePageTest
//
//  Created by Oliver Erxleben on 03.08.15.
//  Copyright © 2015 Hochschule Osnabrück. All rights reserved.
//

import UIKit
import HybridKit
import WebKit

class ViewController: HyKViewController {

    let context = CIContext()
    var image: String = ""
    var timer: NSTimer?
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let messages = NSMutableDictionary()
        messages["takePhoto"] = HyKMessage(identifier: "TakePhoto", type: HyKMessageType.ACTION, handler: takePhotoHandler)
        messages["taskDemo"] = HyKMessage(identifier: "TaskDemo", type: HyKMessageType.TASK, handler: taskDemoHandler)
//
//        let hyk = HybridKit(url: "http://bigmac.mi.ecs.hs-osnabrueck.de/~oerxlebe/mad/single-page/", messageHandlers: messages)
////        let hyk = HybridKit(url: "http://localhost:3000/index.html", messageHandlers: messages)
////        self.view = hyk.webView()
//        
        let conf = HyKConfiguration(url: "http://bigmac.mi.ecs.hs-osnabrueck.de/~oerxlebe/mad/single-page/", loadType: HyKWebViewLoadType.REMOTE, configuration: nil, frame: nil, messages: messages)
        
//        let conf = HyKConfiguration(url: "http://localhost:3000/index.html", loadType: HyKWebViewLoadType.REMOTE, configuration: nil, frame: nil, messages: messages)
        
        configure(conf)
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func someBackgroundTask(timer:NSTimer) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), { () -> Void in
            
            
            let r = Int(arc4random_uniform(10))
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
               
                
                self.sendMessage("TaskDemo", type: HyKMessageCallbackType.PROGRESS, data: "\(r)")
                
            })
        })
    }
    
    func taskDemoHandler(params: AnyObject) -> AnyObject {
        

        if (params as! String == "\"start\"") {
            print("should start")

            timer = NSTimer.scheduledTimerWithTimeInterval(2.0, target: self, selector: "someBackgroundTask:", userInfo: nil, repeats: true)

            self.sendMessage("TaskDemo", type: HyKMessageCallbackType.SUCCESS, data: "started")
        }
        
        if (params as! String == "\"stop\"") {
            timer?.invalidate()
            
            self.sendMessage("TaskDemo", type: HyKMessageCallbackType.SUCCESS, data: "stopped")
        }
        
        return 0;
    }
    
    func takePhotoHandler(params: AnyObject) -> AnyObject{
        if UIImagePickerController.isSourceTypeAvailable(
            UIImagePickerControllerSourceType.Camera) {
                
                let imagePicker = UIImagePickerController()

                
                imagePicker.delegate = self
                imagePicker.sourceType =
                    UIImagePickerControllerSourceType.Camera
                //                    imagePicker.mediaTypes
                imagePicker.allowsEditing = false
                
                self.presentViewController(imagePicker, animated: true,
                    completion: nil)
               // newMedia = true
        } else {
            if UIImagePickerController.isSourceTypeAvailable(
                UIImagePickerControllerSourceType.SavedPhotosAlbum) {
                    let imagePicker = UIImagePickerController()
                    
                    imagePicker.delegate = self
                    imagePicker.sourceType =
                        UIImagePickerControllerSourceType.PhotoLibrary
                    //imagePicker.mediaTypes = [kUTTypeImage as NSString]
                    imagePicker.allowsEditing = false
                    self.presentViewController(imagePicker, animated: true,
                        completion: takePhotoCompletion)
                 //   newMedia = false
            }
        }
        return 0
    }
    
    func takePhotoCompletion() {
        print("taken photo")
    }

}


extension ViewController: UIImagePickerControllerDelegate {
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        
        let randomColor = [kCIInputAngleKey: (Double(arc4random_uniform(360)) / 50)]
        let inputImage = CIImage(image: chosenImage)
        
        // Apply a filter to the image
        let filteredImage = inputImage!.imageByApplyingFilter("CIHueAdjust", withInputParameters: randomColor)
        
        let renderedImage = context.createCGImage(filteredImage, fromRect: filteredImage.extent)
        
        // Create an image to filter
        let representation = UIImagePNGRepresentation(UIImage(CGImage: renderedImage))
        
        
        let base64String = representation!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions.EncodingEndLineWithCarriageReturn)
        

        self.sendMessage("TakePhoto", type: HyKMessageCallbackType.SUCCESS, data: base64String)
        
        
        dismissViewControllerAnimated(true, completion: nil)
        
        

    }
    
   
}

extension ViewController: UINavigationControllerDelegate {
    
}
