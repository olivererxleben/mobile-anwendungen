angular.module('hyk.controllers', [])

.controller('AppCtrl', function($rootScope, $scope, $ionicModal, $timeout, HyKPhotoService) {

        // @TODO Angular rootScope Integration



})

.controller('GalleryCtrl', function($scope, $rootScope, $ionicPopover, HyKPhotoService) {
    $scope.data = HyKPhotoService.all();

    // .fromTemplateUrl() method
    $ionicPopover.fromTemplateUrl('templates/image-upload-popup.html', {
        scope: $scope
    }).then(function(popover) {
        $scope.popover = popover;
    });


    $scope.openPopover = function($event) {
        $scope.popover.show($event);
    };
    $scope.closePopover = function() {
        $scope.popover.hide();
    };
    //Cleanup the popover when we're done with it!
    $scope.$on('$destroy', function() {
        $scope.popover.remove();
    });
    // Execute action on hide popover
    $scope.$on('popover.hidden', function() {
        // Execute action
    });
    // Execute action on remove popover
    $scope.$on('popover.removed', function() {
        // Execute action
    });

    $scope.addButtonClicked = function($event) {

        if (hyk.isAvailable('TakePhoto')) {

            hyk.sendMessage('TakePhoto');
        } else {
            $scope.openPopover($event);
        }
    };

    $scope.addFile = function() {
        var f = document.getElementById('file').files[0],
            r = new FileReader();

        r.onloadend = function(e){
            $scope.img = e.target.result;
            console.log(e.target.result)
        };
        r.readAsBinaryString(f);
    }
})

.controller('ComputationCtrl', function($scope) {

        $scope.startButtonClicked = function() {
            if (hyk.isAvailable('TaskDemo')) {
                hyk.sendMessage('TaskDemo', "start");
            }
        };

        $scope.stopButtonClicked = function() {
            if (hyk.isAvailable('TaskDemo')) {
                hyk.sendMessage('TaskDemo', 'stop');
            }
        };
});
