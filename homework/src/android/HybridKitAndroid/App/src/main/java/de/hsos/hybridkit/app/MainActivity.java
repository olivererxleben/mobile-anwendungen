package de.hsos.hybridkit.app;

import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import de.hsos.hybridkit.message.HyKActionMessage;
import de.hsos.hybridkit.message.HyKTaskMessage;
import de.hsos.hybridkit.message.ResponseType;
import de.hsos.hybridkit.view.HyKWebView;

public class MainActivity extends AppCompatActivity
{

    protected final int TAKE_PHOTO_REQUEST = 111;

    protected SharedPreferences mPreferences;
    protected HyKWebView mWebView;
    protected ProgressBar mProgressBar;


    @Override
    protected void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main );

        mProgressBar = ( ProgressBar ) findViewById( R.id.webview_progressbar );

        mWebView = ( HyKWebView ) findViewById( R.id.main_webview );
        mWebView.setWebContentsDebuggingEnabled( true );
        mWebView.setWebChromeClient( new WebChromeClient()
        {
            @Override
            public void onProgressChanged( WebView view, int newProgress )
            {
            if( newProgress == mProgressBar.getMax() )
            {
                mProgressBar.setVisibility( View.GONE );
            }
            else
            {
                mProgressBar.setVisibility( View.VISIBLE );
            }
            mProgressBar.setProgress( newProgress );
            }
        } );
        mWebView.init();
        // Interestingly this new HyKMessage will be treated as a "MainActivity" by the runtime
        // to enable calling code from the enclosing classe. A simple class comparison is therefore
        // not possible when invoking the message.
        // see: http://blog.andresteingress.com/2011/10/12/anonymous-inner-classes-in-android/
        mWebView.registerMessage( "TakePhoto", new HyKActionMessage()
        {
            @Override
            public void execute()
            {
                Intent intent = new Intent( MediaStore.ACTION_IMAGE_CAPTURE );
                MainActivity.this.startActivityForResult( intent, TAKE_PHOTO_REQUEST );
            }
        } );
        mWebView.registerMessage( "TaskDemo", new HyKTaskMessage()
        {
            @Override
            public void run()
            {
                int i = 1;
                while( !Thread.currentThread().isInterrupted() )
                {
                    try
                    {
                        this.progress( "Hello from Android! " + i );
                        Thread.sleep( 1500 );
                        i++;
                    }
                    catch( InterruptedException e )
                    {
                        // Since interrupt() resets the isInterrupted flag, we have to call it again.
                        Thread.currentThread().interrupt();
                        Log.d( Thread.currentThread().getName(), "Thread got interrupted." );
                    }
                }
                this.complete( "Android task completed." );
            }
        } );

        mPreferences = PreferenceManager.getDefaultSharedPreferences( this );
        final String settingsUrl = mPreferences.getString( SettingssActivity.KEY_WEBVIEWADDRESS, null );

        // Does Java else-if behave like a cascading if-else or like a fall-through???
        if( mWebView.getDefaultUrl() != null )
        {
            mWebView.loadUrl( mWebView.getDefaultUrl() );
        }
        else{
            if( settingsUrl == null || ( settingsUrl != null && settingsUrl.isEmpty() ) )
            {
                AlertDialog dialog = this.getWebViewAddressDialog( new DialogInterface.OnDismissListener()
                {
                    @Override
                    public void onDismiss( DialogInterface dialog )
                    {
                        mWebView.loadUrl( settingsUrl );
                    }
                } );
                dialog.show();
            }
            else
            {
                mWebView.loadUrl( mPreferences.getString( SettingssActivity.KEY_WEBVIEWADDRESS, "" ) );
            }
        }
    }

    @Override
    protected void onActivityResult( int requestCode, int resultCode, Intent data )
    {
        if( requestCode == TAKE_PHOTO_REQUEST )
        {
            if( resultCode == RESULT_OK )
            {
                // Process picture thumbnail
                Bundle extra = data.getExtras();
                Bitmap image = ( Bitmap ) extra.get( "data" );
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                image.compress( Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream );
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                String base64Img = Base64.encodeToString( byteArray, Base64.NO_WRAP );
                try
                {
                    byteArrayOutputStream.close();
                }
                catch( IOException e )
                {
                    e.printStackTrace();
                }
                mWebView.sendResponse( "TakePhoto", ResponseType.Success, base64Img );
            }
            else
            {
                mWebView.sendResponse( "TakePhoto", ResponseType.Failure, null );
            }
        }

        super.onActivityResult( requestCode, resultCode, data );
    }

    private AlertDialog getWebViewAddressDialog( AlertDialog.OnDismissListener onDismissListener )
    {
        final EditText textInput = new EditText( this );
        textInput.setInputType( InputType.TYPE_TEXT_VARIATION_URI );
        final AlertDialog dialog = new AlertDialog.Builder( this, R.style.AppDialogTheme )
                .setPositiveButton( "OK", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick( DialogInterface dialog, int which )
                    {
                        String input = textInput.getText().toString();
                        SharedPreferences.Editor editor = mPreferences.edit();
                        editor.putString( SettingssActivity.KEY_WEBVIEWADDRESS, input );
                        editor.commit();

                        dialog.dismiss();
                    }
                } )
                .setOnDismissListener( onDismissListener )
                .create();

        dialog.setView( textInput, 50, 0, 50, 0 );
        dialog.setTitle( this.getString( R.string.dialog_enter_address ) );
        dialog.setMessage( this.getString( R.string.dialog_enter_address_summ ) );

        return dialog;
    }


    @Override
    public boolean onCreateOptionsMenu( Menu menu )
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate( R.menu.menu_main, menu );

        return true;
    }

    @Override
    public boolean onOptionsItemSelected( MenuItem item )
    {
        int id = item.getItemId();

        if( id == R.id.action_settings )
        {
            startActivity( new Intent( this, SettingssActivity.class ) );
            return true;
        }

        if( id == R.id.action_refresh )
        {
            String address = mPreferences.getString( SettingssActivity.KEY_WEBVIEWADDRESS, "http://localhost:8080" );
            mWebView.loadUrl( address );
            Toast.makeText( this, "Lade Seite von " + address, Toast.LENGTH_SHORT ).show();
        }

        return super.onOptionsItemSelected( item );
    }
}
