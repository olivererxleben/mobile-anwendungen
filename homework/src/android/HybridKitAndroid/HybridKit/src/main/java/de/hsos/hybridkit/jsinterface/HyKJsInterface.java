package de.hsos.hybridkit.jsinterface;

import android.webkit.JavascriptInterface;

import java.io.IOException;
import java.util.List;

import de.hsos.hybridkit.view.HyKWebView;

/**
 * Stellt Funktionen bereit, die der {@link de.hsos.hybridkit.view.HyKWebView} zur Kommunikation
 * mit dem Inhalt ueber JavaScript benoetigt.
 */
public class HyKJsInterface
{
    protected HyKWebView mHyKWebView;

    public HyKJsInterface( HyKWebView view )
    {
        mHyKWebView = view;
    }

    private final static String LOGTAG = "HyKJsInterface";
    /**
     * This is the name which will be used to make the interface accessible by the
     * web application.
     */
    public final static String NAME = "hybridkit";

    /**
     * @param name   Name of the registered {@link de.hsos.hybridkit.message.HyKMessage} that
     *               should be invoke.
     * @param params Additional parameters for the message.
     */
    @JavascriptInterface
    public void sendMessage( String name, String params )
    {
        mHyKWebView.evaluateRequest( name, params );
    }

    /**
     * Configures the bridge between the application and the web application.
     *
     * @param configuration A string containing the HybridKit configuration of the web application.
     * @return A string containing the all the messages that the application allows the
     * web application to invoke.
     */
    @JavascriptInterface
    public String configure( String configuration )
    {
        String allowedMessagesResponse = "";
        try
        {
            List<String> allowedMessagesList = mHyKWebView
                    .configureWebApplication( mHyKWebView.getConfigReader()
                            .readConfig( configuration ) );

            allowedMessagesResponse = mHyKWebView.getConfigReader().genericListToString( allowedMessagesList );
        }
        catch( IOException e )
        {
            e.printStackTrace();
        }

        mHyKWebView.setConfigured( true );
        return allowedMessagesResponse;
    }
}