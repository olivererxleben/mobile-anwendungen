/**
 * Created by Sebastian Kaempfer on 21.07.2015.
 **/

package de.hsos.hybridkit.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.util.Log;
import android.webkit.ValueCallback;
import android.webkit.WebSettings;
import android.webkit.WebView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hsos.hybridkit.R;
import de.hsos.hybridkit.config.ConfigReader;
import de.hsos.hybridkit.config.ConfiguredMessage;
import de.hsos.hybridkit.config.JsonConfigReader;
import de.hsos.hybridkit.exception.HyKException;
import de.hsos.hybridkit.jsinterface.HyKJsInterface;
import de.hsos.hybridkit.message.HyKActionMessage;
import de.hsos.hybridkit.message.HyKMessage;
import de.hsos.hybridkit.message.HyKTaskMessage;
import de.hsos.hybridkit.message.ResponseType;

/**
 * View component for displaying web contents that enhances the underlying {@link WebView}.
 * The enhanced functionality allows communication between the Android application and the
 * web application.
 */
public class HyKWebView extends WebView
{
    private final static String LOGTAG = "HyKWebView";

    /**
     * Indicates if the framework was initialized in the app.
     */
    protected boolean mInitialized;
    /**
     * Indicates if the framework has been configured for message exchange with
     * the currently loaded url.
     */
    protected boolean mConfigured;
    /**
     * Die Standard-URL, die geladen werden soll.
     */
    protected String mDefaultUrl;
    /**
     * Instanz eines {@link HyKJsInterface} zur Kommunikation mit JavaScript der Webseite.
     */
    protected HyKJsInterface mJsInterface;
    /**
     * Implementation instance of a {@link ConfigReader} that evaluates configurations and
     * commands received by the web application.
     */
    protected ConfigReader mConfigReader;
    /**
     * A store for {@link HyKMessage}s
     */
    protected Map<String, ConfiguredMessage> mRegisteredMessages;
    /**
     * A user specific action that will be invoked when the view has been configured successfully.
     */
    protected OnConfiguredListener mOnConfiguredListener;
    /**
     * Thread group for message threads.
     */
    protected ThreadGroup mMessageThreadGroup;

    public HyKWebView( Context context )
    {
        this( context, null );
    }

    public HyKWebView( Context context, AttributeSet attrs )
    {
        this( context, attrs, 0 );
    }

    public HyKWebView( final Context context, AttributeSet attrs, int defStyleAttr )
    {
        super( context, attrs, defStyleAttr );

        TypedArray attributes = context.obtainStyledAttributes( attrs, R.styleable.HyKWebView );
        boolean enableJavascript = attributes.getBoolean( R.styleable.HyKWebView_enableJavaScript, false );
        boolean allowRemoteUrl = attributes.getBoolean( R.styleable.HyKWebView_allowRemoteUrl, false );
        String defaultUrl = attributes.getString( R.styleable.HyKWebView_defaultUrl );
        WebSettings settings = this.getSettings();

        attributes.recycle();

        mRegisteredMessages = new HashMap<String, ConfiguredMessage>();
        mMessageThreadGroup = new ThreadGroup( "HybridKitThreadGroup" );
        mOnConfiguredListener = new OnConfiguredListener()
        {
            @Override
            public void onConfigured()
            {

            }
        };
    }

    public OnConfiguredListener getOnConfiguredListener()
    {
        return mOnConfiguredListener;
    }

    public void setOnConfiguredListener( OnConfiguredListener onConfiguredListener )
    {
        this.mOnConfiguredListener = onConfiguredListener;
    }

    public ConfigReader getConfigReader()
    {
        return mConfigReader;
    }

    public void setConfigReader( ConfigReader mConfigReader )
    {
        this.mConfigReader = mConfigReader;
    }

    public boolean isConfigured()
    {
        return mConfigured;
    }

    public void setConfigured( boolean mConfigured )
    {
        this.mConfigured = mConfigured;
        if( mConfigured == true )
        {
            this.getOnConfiguredListener().onConfigured();
        }
    }

    public String getDefaultUrl()
    {
        return mDefaultUrl;
    }

    public void setDefaultUrl( String mDefaultUrl )
    {
        this.mDefaultUrl = mDefaultUrl;
    }


    /**
     * Initialises the enhanced functionality of this view. Otherwise the view
     * behaves just like an ordinary {@link WebView}.
     */
    public void init()
    {
        Log.d( LOGTAG, "Initialize HyKWebView." );

        // Set default components if not set already.
        if( mConfigReader == null )
        {
            mConfigReader = new JsonConfigReader();
        }

        // We need JavaScript enabled for the framework to work.
        WebSettings settings = this.getSettings();
        settings.setJavaScriptEnabled( true );

        // Allow web application to communicate with the framework.
        super.addJavascriptInterface( new HyKJsInterface( this ), HyKJsInterface.NAME );

        // Framework specific event handling in the web view.
        this.setWebViewClient( new HyKWebViewClient() );

        mInitialized = true;
    }

    /**
     * Checks if the {@link HyKWebView} is initialized.
     *
     * @return true if view is initialized, false otherwise.
     */
    public boolean isInitialized()
    {
        return mInitialized;
    }

    /**
     * Sets the initialization state.
     *
     * @param mInitialized
     */
    protected void setInitialized( boolean mInitialized )
    {
        this.mInitialized = mInitialized;
    }

    /**
     * If the enhanced functionality of the framework has been initialized, we need to take
     * more actions before and after the view has loaded to ensure proper communication between
     * this application and the web application.
     *
     * @param url The url to be loaded into the web view.
     */
    @Override
    public void loadUrl( String url )
    {
        if( mInitialized )
        {
            mConfigured = false;
        }
        super.loadUrl( url );
    }

    /**
     * We need to reserve a specific JavaScript interface object name for the framework.
     *
     * @param object
     * @param name
     * @throws HyKException
     */
    @Override
    public void addJavascriptInterface( Object object, String name )
    {
        if( name.equals( HyKJsInterface.NAME ) )
        {
            Log.e( LOGTAG, String.format( "Interface name %s is reserved for the HybridKit framework. Custom interface will not be added." ) );
            throw new HyKException( "Interface name %s is reserved for the HybridKit framework. Custom interface will not be added." );
        }
        super.addJavascriptInterface( object, name );
    }

    /**
     * Registers a user specific {@link de.hsos.hybridkit.message.HyKMessage}.
     *
     * @param name    The name used to identify the passed {@link de.hsos.hybridkit.message.HyKMessage}.
     * @param message The class of the {@link de.hsos.hybridkit.message.HyKMessage} for invocation.
     */
    public void registerMessage( String name, HyKMessage message )
    {
        if( mRegisteredMessages.get( name ) == null )
        {
            // Hack for configuring information that the task needs when sending progress and completion messages.
            // We need to check for super class because instances which override methods in
            // the enclosing class are treated like the enclosing class at runtime.
            if( message.getClass() == HyKTaskMessage.class || message.getClass().getSuperclass() == HyKTaskMessage.class )
            {
                ( ( HyKTaskMessage ) message ).setMessageName( name );
                ( ( HyKTaskMessage ) message ).setWebView( this );
            }

            ConfiguredMessage configuredMessage = new ConfiguredMessage();
            configuredMessage.setMessage( message );
            configuredMessage.setName( name );
            mRegisteredMessages.put( name, configuredMessage );
        }
    }

    /**
     * Deletes a user specific {@link de.hsos.hybridkit.message.HyKMessage} from the registration.
     *
     * @param name The name used to identify the registered {@link de.hsos.hybridkit.message.HyKMessage}.
     */
    public void unregisterMessage( String name )
    {
        mRegisteredMessages.remove( name );
    }

    /**
     * Gets all registered messages.
     *
     * @return Map of registered messages.
     */
    public Map<String, ConfiguredMessage> getRegisteredMessages()
    {
        return mRegisteredMessages;
    }

    /**
     * Finalizes all framework specific functionality for the currently loaded page.
     */
    public void deactivate()
    {

    }

    /**
     * Will be invoked when the connection between the app and the web application has
     * been configured by the framework.
     * This is a callback method and can be
     */
    public void onConfigured( OnConfiguredListener listener )
    {
        mOnConfiguredListener = listener;
    }

    /**
     * Sends a response message to the web application.
     *
     * @param name         The name of the message the response should be routed to.
     * @param responseType The {@link ResponseType} of the response.
     * @param response     The actual message data.
     */
    public void sendResponse( String name, ResponseType responseType, String response )
    {
        ConfiguredMessage configuredMessage = mRegisteredMessages.get( name );
        if( configuredMessage != null )
        {
            final String callback = configuredMessage.getResponseCallback( responseType );
            if( callback != null )
            {
                /** JavaScript message **/
                final String javaScript = callback + "('" + response + "');";
                /** The action in the handler's run method will be sent to the
                 UI thread's message queue and invoked there. **/
                Handler handler = new Handler( Looper.getMainLooper() );
                handler.post( new Runnable()
                {
                    @Override
                    public void run()
                    {
                        HyKWebView.this.evaluateJavascript( javaScript, new ValueCallback<String>()
                        {
                            @Override
                            public void onReceiveValue( String value )
                            {
                                Log.d( LOGTAG, "Response sent to callback " + callback );
                            }
                        } );
                    }
                } );
            }
        }
    }

    /**
     * Handles a request that is received from the web application.
     *
     * @param name   The name of the message that is invoked by the web application.
     * @param params Additional message parameters.
     */
    public void evaluateRequest( String name, String params )
    {
        ConfiguredMessage configuredMessage = this.mRegisteredMessages.get( name );
        final HyKMessage message = configuredMessage.getMessage();
        String messageType = configuredMessage.getMessageType();
        /*** Handle ACTIONS ***/
        if( messageType.equals( "action" ) )
        {
            Log.d( LOGTAG, "Executing action message " + configuredMessage.getName() );
            ( ( HyKActionMessage ) message ).execute();
        }
        /*** Handle TASKS ***/
        else if( messageType.equals( "task" ) )
        {
            Log.d( LOGTAG, "Executing task message " + configuredMessage.getName() );
            if( params.equals( "start" ) )
            {
                if( !this.isMessageActiveThread( configuredMessage.getName() ) )
                {
                    Thread t = new Thread(
                            mMessageThreadGroup,
                            ( HyKTaskMessage ) message,
                            configuredMessage.getName() );
                    t.start();
                }
                else
                {
                    Log.d( LOGTAG, "Cannot start task " + configuredMessage.getName() + " because it is already started." );
                }
            }
            else if( params.equals( "stop" ) )
            {
                this.tryStopThread( configuredMessage.getName() );
            }
            else
            {
                //Handle other input
            }
        }
        /*** Handle EXTENSIONS (not implemented in prototype) ***/
        else if( messageType.equals( "extension" ) )
        {
            Log.d( LOGTAG, "Executing extension message " + configuredMessage.getName() );
        }
        else
        {

        }
    }

    /**
     * Handles the configuration request of the web application and updates the {@link HyKWebView#mRegisteredMessages}
     * with information where to send responses.
     *
     * @param configuration Configuration of the web application.
     * @return A list of names containing all messages that the framework allows the web application
     * to execute.
     */
    public List<String> configureWebApplication( List<ConfiguredMessage> configuration )
    {
        ArrayList<String> allowedMessages = new ArrayList<String>();
        for( ConfiguredMessage message : configuration )
        {
            ConfiguredMessage registeredMessage = mRegisteredMessages.get( message.getName() );
            if( registeredMessage != null )
            {
                allowedMessages.add( registeredMessage.getName() );
                registeredMessage.setMessageType( message.getMessageType() );
                registeredMessage.setOnSuccessCallback( message.getOnSuccessCallback() );
                registeredMessage.setOnProgressCallback( message.getOnProgressCallback() );
                registeredMessage.setOnFailureCallback( message.getOnFailureCallback() );
            }
        }

        return allowedMessages;
    }

    /**
     * Checks if an instance of a {@link HyKTaskMessage} is currently in the {@link HyKWebView#mMessageThreadGroup}.
     *
     * @param messageName
     * @return true if the task is currently active in the {@link HyKWebView#mMessageThreadGroup},
     * false, otherwise.
     */
    private boolean isMessageActiveThread( String messageName )
    {
        Thread[] threads = new Thread[mMessageThreadGroup.activeCount()];
        mMessageThreadGroup.enumerate( threads );
        for( int i = 0; i < threads.length; i++ )
        {
            int j = 1;
            if( threads[i].getName().equals( messageName ) )
            {
                return true;
            }
        }
        return false;
    }

    /**
     * Tries to stop a thread in the {@link HyKWebView#mMessageThreadGroup}.
     *
     * @param messageName The name of the {@link HyKTaskMessage} to stop.
     */
    private void tryStopThread( String messageName )
    {
        Thread[] threads = new Thread[mMessageThreadGroup.activeCount()];
        mMessageThreadGroup.enumerate( threads );
        for( Thread thread : threads )
        {
            if( thread.getName().equals( messageName ) )
            {
                thread.interrupt();
                Log.d( LOGTAG, "Sent interrupt message for thread " + messageName );
            }
        }
    }
}
