package de.hsos.hybridkit.config;

import java.lang.reflect.Type;

import de.hsos.hybridkit.message.HyKActionMessage;
import de.hsos.hybridkit.message.HyKMessage;
import de.hsos.hybridkit.message.ResponseType;

/**
 * Encapsulates all necessary information to for manage the lifecycle of a
 * specific {@link de.hsos.hybridkit.message.HyKMessage}.
 * <p>
 * First, the user registers his message implementation in the Android app.
 * During the configuration process with the web library, this
 * </p>
 */
public class ConfiguredMessage
{
    /**
     * The name by which the message will be identified.
     */
    protected String mName;
    /**
     * The user specific {@link de.hsos.hybridkit.message.HyKMessage}. When the message is
     * invoked through the web application the framework will execute it.
     */
    protected HyKMessage mMessage;

    /**
     * The type of the message as configured by the web application.
     */
    protected String mMessageType;
    /**
     * The name of the JavaScript function to be called when the message finishes successfully.
     */
    protected String onSuccessCallback;
    /**
     * The name of the JavaScript function to be called the messages progress status changes. This
     * attribute can be used by a {@link de.hsos.hybridkit.message.HyKTaskMessage}.
     */
    protected String onProgressCallback;
    /**
     * The name of the JavaScript function to be called when message fails to start or fails
     * during its execution.
     */
    protected String onFailureCallback;

    public String getName()
    {
        return mName;
    }

    public void setName( String mName )
    {
        this.mName = mName;
    }

    public String getOnProgressCallback()
    {
        return onProgressCallback;
    }

    public void setOnProgressCallback( String onProgressCallback )
    {
        this.onProgressCallback = onProgressCallback;
    }

    public HyKMessage getMessage()
    {
        return mMessage;
    }

    public void setMessage( HyKMessage message )
    {
        this.mMessage = message;
    }

    public String getOnSuccessCallback()
    {
        return onSuccessCallback;
    }

    public void setOnSuccessCallback( String onSuccessCallback )
    {
        this.onSuccessCallback = onSuccessCallback;
    }

    public String getOnFailureCallback()
    {
        return onFailureCallback;
    }

    public void setOnFailureCallback( String onFailureCallback )
    {
        this.onFailureCallback = onFailureCallback;
    }

    public String getMessageType()
    {
        return mMessageType;
    }

    public void setMessageType( String mMessageType )
    {
        this.mMessageType = mMessageType;
    }

    /**
     * Gets the name of the callback function configured for this specific message.
     *
     * @param responseType The response type whose callback should be invoked.
     * @return The name of the callback function. null if none is set or if response type is unknown.
     */
    public String getResponseCallback( ResponseType responseType )
    {
        switch( responseType )
        {
            case Success:
                return this.getOnSuccessCallback();
            case Progress:
                return this.getOnProgressCallback();
            case Failure:
                return this.getOnFailureCallback();
            default:
                return null;
        }
    }
}
