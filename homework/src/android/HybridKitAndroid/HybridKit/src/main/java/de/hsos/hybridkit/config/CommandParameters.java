package de.hsos.hybridkit.config;

/**
 * Value class for encapsulating command parameters that are received alongside
 * requests through the {@link de.hsos.hybridkit.jsinterface.HyKJsInterface}.
 */
public class CommandParameters
{
    public String mCommand;

    public String getCommand()
    {
        return mCommand;
    }

    public void setCommand( String mCommand )
    {
        this.mCommand = mCommand;
    }
}
