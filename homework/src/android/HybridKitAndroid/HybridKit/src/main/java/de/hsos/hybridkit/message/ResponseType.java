package de.hsos.hybridkit.message;

/**
 * Indicates the JavaScript callback a response should be sent to.
 */
public enum ResponseType
{
    Success,
    Progress,
    Failure
}
