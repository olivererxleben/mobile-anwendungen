package de.hsos.hybridkit.config;

import java.io.IOException;
import java.util.List;

public abstract class ConfigReader
{

    /**
     * Reads a configuration string and parses it into an entity.
     *
     * @param config A {@link String} containing the configuration.
     * @return A list of {@link ConfiguredMessage}s containing the web applications configuration.
     * @throws IOException Is thrown if the passed parameter cannot be parsed.
     */
    public abstract List<ConfiguredMessage> readConfig( String config ) throws IOException;

    /**
     * Reads a string of command parameters and parses it into an entity.
     *
     * @param params A {@link String} containing the command parameters.
     * @return A {@link CommandParameters} instance containing commands passed by
     * the web application.
     */
    public abstract CommandParameters readCommandParameters( String params ) throws IOException;

    /**
     * Returns the String representation of {@code list}.
     *
     * @param list The list to parse.
     * @return String representation.
     */
    public abstract <T> String genericListToString( List<T> list );
}
