package de.hsos.hybridkit.message;

import android.os.Handler;
import android.os.Looper;

import de.hsos.hybridkit.view.HyKWebView;

/**
 * Represents a user specific task which is invoked by the framework upon request of the web
 * application. A task runs in the background and may run for an indefinite time span.
 */
public class HyKTaskMessage extends HyKMessage implements Runnable
{
    protected HyKWebView mWebView;
    protected String mMessageName;

    public HyKWebView getWebView()
    {
        return mWebView;
    }

    public void setWebView( HyKWebView mWebView )
    {
        this.mWebView = mWebView;
    }

    public String getMessageName()
    {
        return mMessageName;
    }

    public void setMessageName( String mMessageName )
    {
        this.mMessageName = mMessageName;
    }

    public final void progress( String progressMessage )
    {
        this.sendResponseToUiThread( getMessageName(), ResponseType.Progress, progressMessage );
    }

    public final void complete( String completionMessage )
    {
        this.sendResponseToUiThread( getMessageName(), ResponseType.Success, completionMessage );
    }

    /**
     * Calls to webview methods must be done on the UI thread. Therefore we send
     * the response to the {@link HyKWebView}'s thread message queue.
     */
    private void sendResponseToUiThread( final String messageName, final ResponseType responseType, final String response )
    {
        Handler handler = new Handler( Looper.getMainLooper() );
        handler.post( new Runnable()
        {
            @Override
            public void run()
            {
                HyKTaskMessage.this.getWebView().sendResponse( messageName, responseType, response );
            }
        } );
    }

    @Override
    public void run()
    {

    }
}
