package de.hsos.hybridkit.message;

/**
 * Abstract base class for encapsulation of specific tasks that should be executed by
 * the HybridKit framework.
 */
public abstract class HyKMessage
{
}
