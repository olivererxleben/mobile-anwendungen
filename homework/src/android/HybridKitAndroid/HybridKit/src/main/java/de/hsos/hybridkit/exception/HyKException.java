package de.hsos.hybridkit.exception;

/**
 * Frameworkspezifische {@link Exception}, die im Ausnahmefall geworfen wird.
 */
public class HyKException extends Error
{
    public HyKException( String detailMessage )
    {
        super( detailMessage );
    }

    public HyKException( String detailMessage, Throwable throwable )
    {
        super( detailMessage, throwable );
    }

    public HyKException( Throwable throwable )
    {
        super( throwable );
    }
}
