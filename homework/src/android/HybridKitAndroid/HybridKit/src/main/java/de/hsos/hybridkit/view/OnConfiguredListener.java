package de.hsos.hybridkit.view;

/**
 * Allows users to define custom actions that will be invoked when the framework
 * has successfully configured a connection between the app and the web application.
 */
public interface OnConfiguredListener
{
    void onConfigured();
}
