
package de.hsos.hybridkit.config;

import android.util.JsonReader;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import de.hsos.hybridkit.helper.MessageHelper;

public class JsonConfigReader extends ConfigReader
{
    @Override
    public List<ConfiguredMessage> readConfig( String config ) throws IOException
    {
        List<ConfiguredMessage> configuration = new ArrayList<ConfiguredMessage>();
        StringReader stringReader = new StringReader( config );
        JsonReader jsonReader = new JsonReader( stringReader );

        try
        {
            jsonReader.beginArray();
            while( jsonReader.hasNext() )
            {
                readObject( jsonReader, configuration );
            }
            jsonReader.endArray();

            jsonReader.close();
            return configuration;
        }
        catch( IOException e )
        {
            throw e;
        }
    }

    protected void readObject( JsonReader reader, List<ConfiguredMessage> configuration ) throws IOException
    {
        ConfiguredMessage message = new ConfiguredMessage();
        reader.beginObject();
        while( reader.hasNext() )
        {
            String name = reader.nextName().toLowerCase();
            if( name.equals( "name" ) )
            {
                message.setName( reader.nextString() );
            }
            else if( name.equals( "type" ) )
            {
                message.setMessageType( reader.nextString() );
            }
            else if( name.equals( "onsuccess" ) )
            {
                message.setOnSuccessCallback( reader.nextString() );
            }
            else if( name.equals( "onprogress" ) )
            {
                message.setOnProgressCallback( reader.nextString() );
            }
            else if( name.equals( "onfailure" ) )
            {
                message.setOnFailureCallback( reader.nextString() );
            }
            else
            {
                reader.skipValue();
            }
        }
        reader.endObject();
        configuration.add( message );
    }

    @Override
    public CommandParameters readCommandParameters( String params ) throws IOException
    {
        // Little validation checks
        if( params == null || !params.isEmpty() || params.equals( "undefined" ) )
        {
            return null;
        }
        CommandParameters parameters = new CommandParameters();
        StringReader stringReader = new StringReader( params );
        JsonReader jsonReader = new JsonReader( stringReader );

        jsonReader.beginObject();
        while( jsonReader.hasNext() )
        {
            String name = jsonReader.nextName().toLowerCase();
            if( name.equals( "command" ) )
            {
                parameters.setCommand( jsonReader.nextString() );
            }
            else
            {
                jsonReader.skipValue();
            }
        }
        jsonReader.endObject();

        jsonReader.close();
        return parameters;
    }

    @Override
    public <T> String genericListToString( List<T> list )
    {
        Gson gson = new Gson();
        Type typeToken = new TypeToken<List<T>>()
        {
        }.getType();
        String returnValue = gson.toJson( list, typeToken );
        return returnValue;
    }


}
