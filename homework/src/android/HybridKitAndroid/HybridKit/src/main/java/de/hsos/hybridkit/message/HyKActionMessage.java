package de.hsos.hybridkit.message;

/**
 * Represents a user specific action which is invoked by the framework upon request by the
 * web application.
 */
public abstract class HyKActionMessage extends HyKMessage
{
    /**
     * Encapsulates user specific code. This method is invoked by the framework if a
     * corresponding request is made by the web application.
     */
    public abstract void execute();
}
