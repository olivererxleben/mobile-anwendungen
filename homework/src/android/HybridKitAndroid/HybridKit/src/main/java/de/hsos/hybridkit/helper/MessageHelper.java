package de.hsos.hybridkit.helper;

import java.lang.reflect.Type;

import de.hsos.hybridkit.message.HyKActionMessage;
import de.hsos.hybridkit.message.HyKExtensionMessage;
import de.hsos.hybridkit.message.HyKTaskMessage;

/**
 * Provides helper methods.
 */
public class MessageHelper
{
    /**
     * Maps the class of a {@link de.hsos.hybridkit.message.HyKMessage} to its string representation.
     *
     * @param messageClass The class of the {@link de.hsos.hybridkit.message.HyKMessage}.
     * @return A string representation of the type. null if mapping is not possible.
     */
    public static String mapHyKMessageType( Class messageClass )
    {
        if( messageClass == HyKActionMessage.class )
        {
            return "action";
        }
        else if( messageClass == HyKTaskMessage.class )
        {
            return "task";
        }
        else if( messageClass == HyKExtensionMessage.class )
        {
            return "extension";
        }
        else
        {
            return null;
        }
    }

    /**
     * Maps string representation of a {@link de.hsos.hybridkit.message.HyKMessage} to its class.
     *
     * @param className The string representation of the {@link de.hsos.hybridkit.message.HyKMessage}.
     * @return The corresponding type for the passed string. null if mapping is not possible.
     */
    public static Class mapHyKMessageString( String className )
    {
        if( className.toLowerCase().equals( "action" ) )
        {
            return HyKActionMessage.class;
        }
        else if( className.toLowerCase().equals( "task" ) )
        {
            return HyKTaskMessage.class;
        }
        else if( className.toLowerCase().equals( "extension" ) )
        {
            return HyKExtensionMessage.class;
        }
        else
        {
            return null;
        }
    }
}
