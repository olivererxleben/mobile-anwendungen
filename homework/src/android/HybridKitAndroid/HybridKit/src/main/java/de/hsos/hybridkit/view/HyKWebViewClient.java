
package de.hsos.hybridkit.view;

import android.graphics.Bitmap;
import android.util.Log;
import android.webkit.*;
import android.widget.Toast;

import de.hsos.hybridkit.view.HyKWebView;

public class HyKWebViewClient extends WebViewClient {

    private final static String LOGTAG = "HyKWebViewClient";


    /**
     * We want to load all sites in the web view.
     * @param view
     * @param url
     * @return
     */
    @Override
    public boolean shouldOverrideUrlLoading( WebView view, String url ) {
        return false;
    }

    @Override
    public void onPageFinished( WebView view, String url )
    {
        if( view.getClass() == HyKWebView.class ){
            this.initializeWebApp( ( HyKWebView)view, url );
        }
    }

    /**
     * Sends an initialization command to the web application.
     * @param view
     * @param url
     */
    protected void initializeWebApp( HyKWebView view, String url){
        view.evaluateJavascript( "hyk.init()", new ValueCallback<String>()
        {
            @Override
            public void onReceiveValue( String value )
            {
                Log.d( LOGTAG, "Sent init command to web application." );
            }
        } );
    }
}
