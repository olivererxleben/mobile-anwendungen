/**
 * HyK - the HybridKit Web Object
 *
 * @param {Array | Object} config - a config JSON Array
 * */
function HyK(config) {

    // internal configuration object
    this._configuration = [];

    this._OS = this.checkOS();

    switch(checkType(config)) {
        case "Object":
            this._configuration.push(config);
            break;
        case "Array":
            for (var i = 0; i< config.length; i++) {
                this._configuration.push(config[i]);
            }
            break;

        // add more cases, "String" e.g.
        default:
            // ignore everything else
            // TODO: inform user about unreadable config?
            console.log("hyk: could not read hyk-configuration."); 
            break;
    }

    /**
     * checks type for given object
     * @param {Object} object to typeCheck
     * @return {String | false} String of type or false if  type not found.
     */
    function checkType(object) {

        var arrayConstructor = [].constructor;
        var objectConstructor = {}.constructor;

        if (object === null) {
            return "null";
        }
        else if (object === undefined) {
            return "undefined";
        }
        else if (object.constructor === arrayConstructor) {
            return "Array";
        }
        else if (object.constructor === objectConstructor) {
            return "Object";
        }
        else {
            return false;
        }
    }
}

/**
 * Sets or Gets the actual configuration object
 * @param config
 **/
HyK.prototype.config = function(config) {
    if (config != null) {
        this._configuration = config
    }
    return this._configuration;
};

/**
 * Little hack for Android: During configuration
 * the application returns a list of allowed messages.
 * iOS achieves this through its message handler array.
 **/
HyK.prototype._allowedMessages = [];

/**
 * determines the OS
 * returns 'iOS', 'Android' or 'unknown'
 *
 * @returns {String}
 */
HyK.prototype.checkOS = function() {
    var userAgent = navigator.userAgent || navigator.vendor;

    if( userAgent.match( /iPad/i ) || userAgent.match( /iPhone/i ) || userAgent.match( /iPod/i ) )
    {
        return 'iOS';
    }
    else if( userAgent.match( /Android/i ) )
    {
        return 'Android';
    }
    else
    {
        return 'unknown';
    }
};

/**
 * checks if function is available in hybrid kit
 * @param name
 * @returns {boolean}
 */
HyK.prototype.isAvailable = function(name) {
    var ret = false;

    switch(this._OS) {
        case 'iOS':

            if (typeof webkit !== "undefined" && webkit.messageHandlers[name]) {
                this._configuration.forEach(function(el) {
                    console.log(el);
                    if (el.name == name) {

                        ret =  true;

                    }
                });
            }

            break;

        case "Android":
            
            if (typeof hybridkit !== 'undefined' && this._allowedMessages.indexOf(name) >= 0) {
                
                ret =  true;

            } 

            break;
        default: break;
    }

    return ret;
};

/**
 * Initializes HybridKit connection
 * sends configuration to native App
 **/
HyK.prototype.init = function() {

    switch(this._OS) {
        case "iOS":
            this.sendMessage('initCallback', this._configuration);
            break;

        case "Android":
            var configString = JSON.stringify(this._configuration);
            this._allowedMessages = hybridkit.configure(configString);
            break;

        default: break;
    }
};

/**
 * Sends a message to the native application
 **/
HyK.prototype.sendMessage = function(name, msg) {

    switch(this._OS) {
        case "iOS":
            webkit.messageHandlers[name].postMessage(JSON.stringify(msg));
            break;
        case "Android":
            // No JSON.stringify() for the param string because the method puts
            // quotes around the string.
            hybridkit.sendMessage( name, msg );
            break;
        default: break;
    }

};
