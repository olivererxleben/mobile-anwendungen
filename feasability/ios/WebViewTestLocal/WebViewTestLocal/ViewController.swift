//
//  ViewController.swift
//  WebViewTestLocal
//
//  Created by Oliver Erxleben on 14.03.15.
//  Copyright (c) 2015 GAD eG. All rights reserved.
//

import UIKit
import WebKit

class ViewController: UIViewController, WKNavigationDelegate, WKScriptMessageHandler, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    var webView = WKWebView()
    var contentController = WKUserContentController()
     var newMedia: Bool?
    
    @IBOutlet weak var webviewContainer: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        //var path = NSBundle.mainBundle().pathForResource("index", ofType: "html")
        //var url = NSURL(fileURLWithPath: path!)
        //var request = NSURLRequest(URL: url!)
        //var text = String(contentsOfFile: path!, encoding: NSUTF8StringEncoding, error: nil)
        let theConfiguration = WKWebViewConfiguration()
        let initScript = WKUserScript(source: "HyK();", injectionTime: WKUserScriptInjectionTime.AtDocumentEnd, forMainFrameOnly: true)
        
        contentController.addUserScript(initScript)
        
        contentController.addScriptMessageHandler(self, name: "callbackHandler")
        contentController.addScriptMessageHandler(self, name: "takePhoto")
        
        // copy all resource files to tmp directory where WKWebView can find it
        theConfiguration.userContentController = contentController
        var filepath = NSBundle.mainBundle().pathForResource("index", ofType: "html")

        do {
            try filepath = self.pathForBuggyWKWebView(filepath)
            var imagepath = NSBundle.mainBundle().pathForResource("background", ofType: "png")
            
            
           try imagepath = pathForBuggyWKWebView(imagepath)
            
            var jspath = NSBundle.mainBundle().pathForResource("move.min", ofType: "js")
            try jspath = pathForBuggyWKWebView(jspath)
        }  catch  {
            // catching errors
        }

        webView.configuration
        
        webView = WKWebView(frame: self.view.frame, configuration: theConfiguration)
        webView.navigationDelegate = self
        //webView.loadRequest(request)
        //webView.loadHTMLString(text!, baseURL: url)
        let req = NSURLRequest(URL: NSURL.fileURLWithPath(filepath!))
        webView.loadRequest(req)
        self.webviewContainer.addSubview(webView)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func userContentController(userContentController: WKUserContentController, didReceiveScriptMessage message: WKScriptMessage) {
        print(message)
        
        if(message.name == "callbackHandler") {
            print("JavaScript is sending a message \(message.body)")
                    }
        
        if(message.name == "takePhoto") {
            if UIImagePickerController.isSourceTypeAvailable(
                UIImagePickerControllerSourceType.Camera) {
                    
                    let imagePicker = UIImagePickerController()
                    
                    imagePicker.delegate = self
                    imagePicker.sourceType =
                        UIImagePickerControllerSourceType.Camera
                    //                    imagePicker.mediaTypes
                    imagePicker.allowsEditing = false
                    
                    self.presentViewController(imagePicker, animated: true,
                        completion: nil)
                    newMedia = true
            } else {
                if UIImagePickerController.isSourceTypeAvailable(
                    UIImagePickerControllerSourceType.SavedPhotosAlbum) {
                        let imagePicker = UIImagePickerController()
                        
                        imagePicker.delegate = self
                        imagePicker.sourceType =
                            UIImagePickerControllerSourceType.PhotoLibrary
                        //imagePicker.mediaTypes = [kUTTypeImage as NSString]
                        imagePicker.allowsEditing = false
                        self.presentViewController(imagePicker, animated: true,
                            completion: nil)
                        newMedia = false
                }
            }

        }
    }
    
    func webView(webView: WKWebView, didCommitNavigation navigation: WKNavigation!) {
        print("committing")
        
    }
    
    func webView(webView: WKWebView, didFinishNavigation navigation: WKNavigation!) {
        print("finished nav")
        
        
    }
    
    
    func pathForBuggyWKWebView(filePath: String?) throws -> String? {
        let fileMgr = NSFileManager.defaultManager()
        let tmpPath = NSTemporaryDirectory().stringByAppendingPathComponent("www")
        
        try fileMgr.createDirectoryAtPath(tmpPath, withIntermediateDirectories: true, attributes: nil)
        
        let dstPath = tmpPath.stringByAppendingPathComponent(filePath!.lastPathComponent)
        if !fileMgr.fileExistsAtPath(dstPath) {
            
           
            try fileMgr.copyItemAtPath(filePath!, toPath: dstPath)
           
        }
        return dstPath
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
       // let mediaType = info[UIImagePickerControllerMediaType] as! String
        
        self.dismissViewControllerAnimated(true, completion: nil)
       
            let image = info[UIImagePickerControllerOriginalImage]
                as! UIImage
            // should call it back to WebView        }
    }
    
    
//    func image(image: UIImage, didFinishSavingWithError error: NSErrorPointer, contextInfo:UnsafePointer<Void>) {
//        
//        if error != nil {
//            let alert = UIAlertController(title: "Save Failed",
//                message: "Failed to save image",
//                preferredStyle: UIAlertControllerStyle.Alert)
//            
//            let cancelAction = UIAlertAction(title: "OK",
//                style: .Cancel, handler: nil)
//            
//            alert.addAction(cancelAction)
//            self.presentViewController(alert, animated: true,
//                completion: nil)
//        }
//    }
}

extension WKUIDelegate {
    
}