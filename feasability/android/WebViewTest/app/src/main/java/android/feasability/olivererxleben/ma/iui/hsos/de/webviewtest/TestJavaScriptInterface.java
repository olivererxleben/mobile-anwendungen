package android.feasability.olivererxleben.ma.iui.hsos.de.webviewtest;

import android.content.Context;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

public class TestJavaScriptInterface {

    private Context context;

    public TestJavaScriptInterface(Context con) {
        this.context = con;
    }

    @JavascriptInterface
    public void showToast(String toast) {
        Toast.makeText(context, toast, Toast.LENGTH_SHORT).show();
    }
}
