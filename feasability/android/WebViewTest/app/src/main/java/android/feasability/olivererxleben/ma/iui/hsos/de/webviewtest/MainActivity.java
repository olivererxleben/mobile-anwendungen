package android.feasability.olivererxleben.ma.iui.hsos.de.webviewtest;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;

public class MainActivity extends Activity {

    WebSettings wSettings;

    @SuppressLint({"SetJavaScriptEnabled", "JavascriptInterface"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        WebView wv = (WebView) findViewById(R.id.webView);
        wSettings = wv.getSettings();
        wSettings.setJavaScriptEnabled(true);


        /**
         * <b> Support Classes For WebView </b>
         */
        //WebClientClass webViewClient = new WebClientClass();
        //wv.setWebViewClient(webViewClient);
        WebChromeClient webChromeClient = new WebChromeClient();
        wv.setWebChromeClient(webChromeClient);

        wv.addJavascriptInterface(new TestJavaScriptInterface(this), "Android");

       // wv.loadUrl("file:///android_asset/index.html"); // 'android_asset' is the assets folder => dont know why
        wv.loadUrl("http://www.hs-osnabrueck.de");

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}


