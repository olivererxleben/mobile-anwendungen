package de.hsos.zinsrechner;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import interests.Interests;


public class ShowActivity extends Activity {

    View layout;

    public static final String AMOUNT_KEY = "amount";
    public static final String INTEREST_KEY = "interest";
    public static final String DURATION_KEY = "duration";
    public static final String ACCUMULATE_KEY = "accumulate";

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show);

        setTitle("Ergebnis");
        layout = findViewById(R.id.activity_show);
        final Bundle extras = getIntent().getExtras();
        if ((extras != null) &&
                extras.containsKey(AMOUNT_KEY) &&
                extras.containsKey(INTEREST_KEY) &&
                extras.containsKey(ACCUMULATE_KEY) &&
                extras.containsKey(DURATION_KEY)) {
            int max_y = extras.getInt(DURATION_KEY);
                for (int y = 1; y <= max_y; y++) {
                    addResult(
                        extras.getFloat(AMOUNT_KEY),
                        extras.getFloat(INTEREST_KEY),
                        y,
                        extras.getBoolean(ACCUMULATE_KEY)
                    );
                }
            }
        }

    private void addResult (float a, float ir, int duration, boolean acc) {
        Interests in = new Interests (a, ir, duration, acc);
        System.out.println ("Enter TextVeiw: " + a + ", " + ir + ","+ duration+","+acc);
        TextView tv = new TextView(getApplicationContext());
        tv.setTextColor(getResources().getColor(R.color.black));
        String result_str = "Kapital nach " + duration;

        if (duration == 1)
            result_str += " Jahr: " + in.getResult();
        else
            result_str += " Jahren: " + in.getResult();

        tv.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        tv.setText( result_str);
        ((LinearLayout) layout).addView(tv);
    }

}
