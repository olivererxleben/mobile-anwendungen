package de.hsos.zinsrechner;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import interests.Interests;


public class MainActivity extends ActionBarActivity {

    int duration = 1;
    float amount = (float) 100.0;
    float interest = (float) 1.0;
    String[] interests;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        interests = getResources().getStringArray(R.array.interest_values);
        Spinner spinner = (Spinner) this.findViewById(R.id.interest);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
            String interest_str = interests[pos]; interest_str = interest_str.replace(',', '.');
            interest = Float.parseFloat(interest_str);
        }

        public void onNothingSelected(AdapterView parent) {
              // Do nothing.
          }
        });
        Button buttonCompute = (Button) findViewById(R.id.button1);
        handle_click(buttonCompute);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void handle_click(Button buttonCompute) {
        final Activity start_activity = this;
        buttonCompute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /* Betrag einlesen */
                final EditText amount_str = (EditText) findViewById(R.id.editText1);
                amount = Float.parseFloat(amount_str.getText().toString());

                /* Betrag einlesen */
                final EditText duration_str = (EditText) findViewById(R.id.editText2);
                duration = Integer.parseInt(duration_str.getText().toString());
                boolean accumulate = true;
                final RadioGroup rg = (RadioGroup) findViewById(R.id.radioGroup1);

                switch (rg.getCheckedRadioButtonId()) {
                    case R.id.radio0:
                        accumulate = true;
                        break;
                    case R.id.radio1:
                        accumulate = false;
                        break;
                    default:
                }

                Interests in = new Interests(amount, interest, duration, accumulate);
                float result = in.getResult();
                final TextView result_label = (TextView) findViewById(R.id.textView4);

                String result_str = "Kapital nach " + duration;
                if (duration == 1)
                    result_str += " Jahr: " + result;
                else
                    result_str += " Jahren: " + result;
                result_label.setText(result_str);

                final Intent intent = new Intent(start_activity, ShowActivity.class);
                intent.putExtra(ShowActivity.AMOUNT_KEY, amount);
                intent.putExtra(ShowActivity.DURATION_KEY, duration);
                intent.putExtra(ShowActivity.INTEREST_KEY, interest);
                intent.putExtra(ShowActivity.ACCUMULATE_KEY, accumulate);
                startActivity(intent);
            }
        });
    }
}
