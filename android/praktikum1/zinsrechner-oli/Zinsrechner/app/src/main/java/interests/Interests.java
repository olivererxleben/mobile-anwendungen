package interests;

/**
 * Created by olivererxleben on 19.03.15.
 */
public class Interests {
    private float amount = (float) 100.0;
    private float interest_rate = (float) 1.0;
    private int duration = 1;
    private boolean accumulate = true;

    public Interests(float a, float ir, int dur, boolean acc) {
        amount = a;
        interest_rate = ir;
        duration = dur;
        accumulate = acc;
    }

    public float getResult() {
        float result = amount;

        float interests = 0;
        for (int i = 1; i <= duration; i++) {
            if (accumulate)
                result += result * interest_rate / 100.0;
            else
                result += amount * interest_rate / 100.0;
        }

        return result;
    }
}
