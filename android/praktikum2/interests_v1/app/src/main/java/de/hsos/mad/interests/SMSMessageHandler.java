package de.hsos.mad.interests;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.widget.Toast;

public class SMSMessageHandler extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Toast.makeText(context, "SMS erhalten", Toast.LENGTH_SHORT).show();
        processTextSMS(context, intent);

    }

    /**
     * processes received SMS text
     * @param context Context of Application
     * @param intent actual Intent
     */
    private void processTextSMS(Context context, Intent intent) {
        final Bundle data = intent.getExtras();

        if (data != null) {
            final Object[] pduUnits = (Object[]) data.get("pdus");
            for (Object pdu : pduUnits) {
                SmsMessage sms;
                sms = SmsMessage.createFromPdu((byte[]) pdu);
                if (sms != null) {
                    String msgBody = sms.getMessageBody();
                    if (msgBody != null) {
                        if (msgBody.equals("egg")) {
                            Intent i = new Intent(context, EasterEggActivity.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(i);
                        } else {
                            showInterestsRate(context, msgBody);
                        }
                    }
                }
            }
        }
    }

    /**
     * Parses smsMessage, validates and Displays a Toast on Screen
     * @param context Context of Application
     * @param smsMsgBody the Message to parse
     */
    private void showInterestsRate(Context context, String smsMsgBody) {
        String[] tokens = smsMsgBody.split("#");

        if (tokens.length != 2) {
            System.err.println("SMS formatting Error: " + tokens.length + " elements, but should be 2.");
        }

        try {
            final String durationStr = tokens[0];
            final String interestStr = tokens[1];

            String message = "Neuer Zins: " + interestStr + " | für " + durationStr + " Jahr(e). ";

            Toast.makeText(context, message, Toast.LENGTH_LONG).show();

            // TODO: save new interest and duration
        } catch (Exception e) {
            System.err.println("SMS formatting error. Text: " + smsMsgBody + " | error: " + e.toString());
        }
    }
}
