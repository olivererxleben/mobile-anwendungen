package de.hsos.mad.interests;

import android.app.backup.BackupAgentHelper;
import android.app.backup.SharedPreferencesBackupHelper;


public class CustomBackupAgent extends BackupAgentHelper {
    // An arbitrary string used within the BackupAgentHelper implementation to
    // identify the SharedPreferencesBackupHelper's data.
    static final String PREFS_BACKUP_KEY = "customPrefs";

    // The names of the SharedPreferences groups that the application maintains.  These
    // are the same strings that are passed to getSharedPreferences(String, int).
    static final String PREFS_INTEREST = "pref_interest";
    static final String PREFS_ACCUMULATE = "pref_accumulate";

    // Allocate a helper and add it to the backup agent
    @Override
    public void onCreate() {
        SharedPreferencesBackupHelper helper = new SharedPreferencesBackupHelper(this, PREFS_INTEREST, PREFS_ACCUMULATE);
        addHelper(PREFS_BACKUP_KEY, helper);
        //FileBackupHelper helper = new FileBackupH elper(this, HIGH_SCORES_FILENAME);
        //addHelper(FILES_BACKUP_KEY, helper);
    }
}