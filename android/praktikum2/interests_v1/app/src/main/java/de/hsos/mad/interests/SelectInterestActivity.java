package de.hsos.mad.interests;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Locale;


public class SelectInterestActivity extends Activity {

    final int TRANSFORM_DIVISOR = 4;
    float interest = (float) 1.0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_interest);

        final SeekBar seekBar = (SeekBar) this.findViewById(R.id.interest_seekbar);
        seekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                // Der Wertebereich der SeekBar geht von 0 bis maximal 100 mit Intervall von 1.
                // Bei anderer Skalierung muss der Wertebereich dementsprechend angepasst werden.
                // Hier: 0 bis 2 in Intervallen von 0.25
                float transformedValue = (float) progress / TRANSFORM_DIVISOR;
                updateInterestRate(transformedValue);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        final Button selectBtn = (Button) this.findViewById(R.id.select_interest_btn);
        selectBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent returnIntent = new Intent();
                String formattedResult = String.format(Locale.US,"%.2f",interest);
                returnIntent.putExtra("result",formattedResult);
                setResult(RESULT_OK,returnIntent);
                finish();
            }
        });

        final Button getFromWebBtn = (Button) this.findViewById(R.id.select_interest_from_web_btn);
        getFromWebBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getInterestRateFromWeb();
            }
        });
    }

    @Override
    public void onResume(){
        super.onResume();
        this.applyPreferences();
    }

    /**
     * Aktualisiert den Zinssatz entsprechend einem übergebenen Wert
     * @param interest Übergebener Zinssatz
     */
    private void updateInterestRate(float interest){
        this.interest = interest;

        String transformedValueText = String.format("%.2f", interest);
        final TextView interestTextView = (TextView) this.findViewById(R.id.interest_seekbar_value);
        String chosenInterestStr = this.getString(R.string.chosen_interest);
        interestTextView.setText(chosenInterestStr + " " + transformedValueText);
    }

    /**
     * Aktualisiert den Schieberegler entsprechend einem übergebenem Zinssatzwert
     * @param interest Übergebener Zinssatz
     */
    private void updateSeekBar(float interest){
        final SeekBar seekBar = (SeekBar) this.findViewById(R.id.interest_seekbar);
        int progress = (int) (interest * TRANSFORM_DIVISOR);
        seekBar.setProgress(progress);
    }

    /**
     * Wendet gespeicherte Benutereinstellungen an
     */
    private void applyPreferences(){
        final SharedPreferences prefs = AppPreferencesActivity.getAppPreferences(this);
        final String interest_str = prefs.getString("pref_interest", "1,0");

        final SeekBar seekBar = (SeekBar) this.findViewById(R.id.interest_seekbar);
        float interestVal = Float.valueOf(interest_str.replace(",", "."));
        int transformedValue = (int) (interestVal * TRANSFORM_DIVISOR);
        seekBar.setProgress(transformedValue);
    }

    /**
     * Beziehet den Zinssatz von einer Webressource
     */
    private void getInterestRateFromWeb(){

//      Für einige Tage wird kein korrekter Wert zurückgeliefert. Deshalb wird hier ein festes Datum verwendet.
//        String url = "http://webservices.lb.lt//LBInterestRates/LBInterestRates.asmx/getLBInterestRatesByDate?Date=";
//        String completeUrl = url + dateString;
        String completeUrl = "http://webservices.lb.lt//LBInterestRates/LBInterestRates.asmx/getLBInterestRatesByDate?Date=2014-03-20";
        final GetInterestRateAsyncTask asyncTask = new GetInterestRateAsyncTask(completeUrl,this);
        asyncTask.execute();
    }

    /**
     * Klasse zum Ausführen des REST-Http-Request zum Beziehen des Zinssatzes
     */
    private class GetInterestRateAsyncTask extends AsyncTask<Void,Void,String> {

        final String LOG_TAG = GetInterestRateAsyncTask.class.getName();
        ProgressDialog mProgressDialog;
        String mUrl;
        Context mCallerActivityContext;

        public GetInterestRateAsyncTask(String url, Context callerActivityContext){
            this.mUrl = url;
            this.mCallerActivityContext = callerActivityContext;
            String dialogMsg = "Beziehe Daten von: ";
            dialogMsg = mUrl != null ? dialogMsg + mUrl : dialogMsg;
            this.mProgressDialog = new ProgressDialog(mCallerActivityContext);
            this.mProgressDialog.setTitle("Zinssatz per HTTP beziehen");
            this.mProgressDialog.setMessage(dialogMsg);
            this.mProgressDialog.setIndeterminate(true);
            this.mProgressDialog.setCancelable(false);
        }

        @Override
        protected String doInBackground(Void... params) {
            ConnectivityManager cm = (ConnectivityManager) mCallerActivityContext.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
            if(!isConnected){
                Log.w(LOG_TAG, "No connected to a network");
                return null;
            }
            Log.w(LOG_TAG, "Connected via "+activeNetwork.getTypeName());

            String result;
            BufferedInputStream inputStream;
            try {
                // Aktuellen Zinssatz per Http abrufen
                URL url = new URL(mUrl);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                inputStream = new BufferedInputStream(urlConnection.getInputStream());

                // Antwort XML parsen
                XmlPullParserFactory pullParserFactory;
                pullParserFactory = XmlPullParserFactory.newInstance();
                XmlPullParser parser = pullParserFactory.newPullParser();

                parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
                parser.setInput(inputStream, null);
                result = parseResultXml(parser);

                inputStream.close();
            } catch (IOException e) {
                Log.e(LOG_TAG,"IO exception",e);
                result = null;
            }
            catch (XmlPullParserException e){
                Log.e(LOG_TAG, "Xml parsing exception", e);
                result = null;
            }

            Log.w(SelectInterestActivity.class.getName(), "Return value: "+result);
            return result;
        }

        @Override
        protected void onPostExecute(String result){
            mProgressDialog.dismiss();
            if(result == null){
                Toast.makeText(SelectInterestActivity.this,"Kein Ergebnis erhalten.",Toast.LENGTH_LONG).show();
            }
            else{
                Toast.makeText(SelectInterestActivity.this,"Ergebnis: "+result,Toast.LENGTH_SHORT).show();
                float resultValue = Float.valueOf(result);
                updateInterestRate(resultValue);
                updateSeekBar(resultValue);
            }
        }

        @Override
        protected void onPreExecute() {
           mProgressDialog.show();
        }

        /**
         * Findet das erste <rate>-Element im Http-Request-Ergebnis und gibt dessen Wert zurück.
         * @param parser Der Parser mit dem zu durchsuchenden Stream
         * @return Den Wert des ersten <rate>-Elements oder null wenn keines gefunden wird.
         */
        private String parseResultXml(XmlPullParser parser) throws XmlPullParserException, IOException {
            String result = null;
            int eventType = parser.getEventType();

            String tagName;
            while( eventType!= XmlPullParser.END_DOCUMENT) {
                if(eventType != XmlPullParser.START_TAG){
                    eventType = parser.next();
                    continue;
                }

                tagName = parser.getName();
                if( tagName.equals("rate")) {
                    result = parser.nextText();
                    break;
                }

                eventType = parser.next();
            }

            return result;
        }
    }
}
