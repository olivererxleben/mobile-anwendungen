package de.hsos.mad.interests;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceActivity;

public class AppPreferencesActivity extends PreferenceActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.addPreferencesFromResource(R.xml.app_preferences);
    }

    public static SharedPreferences getAppPreferences(Context ctx) {
        return ctx.getSharedPreferences(ctx.getPackageName() + "_preferences", MODE_PRIVATE);
    }
}