package de.hsos.mad.interests;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.webkit.WebView;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by olivererxleben on 26.03.15.
 */
public class HelpActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState); setContentView(R.layout.activity_help);
        final WebView view = (WebView) findViewById (R.id.webview); view.getSettings().setJavaScriptEnabled (true); initializeWebKit (view, this);
        view.bringToFront();
    }
    private void initializeWebKit (WebView view, Context context) {
        final String mimeType = "text/html";
        final String encoding = "UTF-8";
        String htmldata;
        int html_help = R.raw.help_complete;
        InputStream is = context.getResources().openRawResource(html_help);

        try {
            if (is != null && is.available() > 0) {
                final byte[] bytes = new byte[is.available()];
                is.read(bytes);
                htmldata = new String(bytes);
                view.loadDataWithBaseURL(null, htmldata, mimeType, encoding, null);
            }
        } catch (IOException e){}
    }
}
