package de.hsos.mad.interests;

import android.app.Activity;
import android.app.backup.BackupManager;
import android.app.backup.RestoreObserver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

public class MainActivity extends Activity {
    final int CHOOSE_INTEREST_REQUEST_CODE = 1;

    int duration = 1;
	float amount = (float) 100.0;
	float interest = (float) 1.0;

    TextView selectedInterestTextView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

        selectedInterestTextView = (TextView) this.findViewById(R.id.selected_interest_txt);
        Button selectInterestsBtn = (Button) this.findViewById(R.id.select_interest_btn);
        selectInterestsBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                getInterestRateSelection();
            }
        });

		Button buttonCompute = (Button) findViewById(R.id.button1);
		handle_click(buttonCompute);
	}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.end_app:
                finish();
                break;
            case R.id.help:
                Intent intent = new Intent(this, HelpActivity.class);
                startActivity(intent);
                break;
            case R.id.preferences:
                Intent prefIntent = new Intent(this, AppPreferencesActivity.class);
                startActivity(prefIntent);
            case R.id.backup_prefs:
                backup();
                break;
            case R.id.restore_prefs:
                restore();
                break;
            default:
        }

        return true;
    }

    @Override
    public void onResume() {
        super.onResume();
        applyPreferences();
        System.out.println("RESUME");
    }

    @Override
    public void onPause() {
        super.onPause();
        writePreferences();
        System.out.println("PAUSE");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if( resultCode == RESULT_OK && requestCode == this.CHOOSE_INTEREST_REQUEST_CODE){
            // Setze Zinssatz entsprechend empfangenen Ergebnis
            if( data.hasExtra("result")){
                // Da nach onActivityResult der Erstellungslebenszyklus (ggfs. komplett) durchlaufen wird,
                // müssen wir das Ergebnis in den Einstellungen speichern
                String result = data.getStringExtra("result");
                this.updateInterestRate(result);
                this.writePreferences();
            }
        }
    }

    /***
     * Aktualisiert die Anzeige mit einem übergebenen Zinssatz, das zugehörige Tag mit Wert und die Klassenvariable
     * @param interestDisplayValue Der Wert des Zinssatzes
     */
    private void updateInterestRate(String interestDisplayValue){
        TextView selectedInterestTxtView = (TextView) this.findViewById(R.id.selected_interest_txt);
        selectedInterestTxtView.setText(String.format("Gewählter Zinssatz: %s",interestDisplayValue));
        selectedInterestTxtView.setTag(R.id.INTEREST_VALUE_TAG,interestDisplayValue);
        this.interest = Float.valueOf(interestDisplayValue);
    }

    /***
     * Wendet gespeicherte Benutzereinstellungen bei Applikationsstart oder -wiederaufruf an
     */
    private void applyPreferences() {
        final SharedPreferences prefs = AppPreferencesActivity.getAppPreferences(this);
        final boolean accumulate = prefs.getBoolean("pref_accumulate", false);
        final String interest_str = prefs.getString("pref_interest", "1,0");

        // Akkumulation
        final RadioGroup rg = (RadioGroup) findViewById(R.id.radioGroup1);
        if (accumulate) {
            rg.check(R.id.radio0);
        }
        else {
            rg.check(R.id.radio1);
        }

        // Zinssatz
        this.updateInterestRate(interest_str.replace(",", "."));
    }

    /***
     * Speichert Benutzereinstellungen beim Verlassen der Anwendung
     */
    private void writePreferences() {
        // Akkumulation
        boolean accumulate = true;
        final RadioGroup rg = (RadioGroup) findViewById(R.id.radioGroup1);
        switch (rg.getCheckedRadioButtonId()) {
            case R.id.radio0:
                accumulate = true;
                break;
            case R.id.radio1:
                accumulate = false;
                break;
            default:
        }

        // Zinssatz
        String interest_str = (String) this.selectedInterestTextView.getTag(R.id.INTEREST_VALUE_TAG);

        // Speichern
        final SharedPreferences prefs = AppPreferencesActivity.getAppPreferences(this);
        final SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("pref_accumulate", accumulate);
        editor.putString("pref_interest", interest_str.replace(".", ","));
        editor.commit();
    }

    /***
     * Sichert die SharedPreferences auf dem Google Cloud Server.
     */
    private void backup() {
        System.out.println("BACKUP");
        final BackupManager bm = new BackupManager(this);
        bm.dataChanged();
    }

    /***
     * Stellt die SharedPreferences vom Google Cloud Server her.
     */
    private void restore() {
        System.out.println("RESTORE");
        final BackupManager bm = new BackupManager(this);
        bm.requestRestore(new RestoreObserver() {
            // Do nothing.
        });
    }

    /***
     * Ruft eine neue Aktivität auf, in der der Zinssatz festgelegt werden kann
     */
    private void getInterestRateSelection(){
        Intent chooseInterestIntent = new Intent(this, SelectInterestActivity.class);
        this.startActivityForResult(chooseInterestIntent, this.CHOOSE_INTEREST_REQUEST_CODE);
    }

	/**
	 * Erzeugung eines Listeners fuer Button-Clicks.
	 */
	private void handle_click (Button buttonCompute) {
		buttonCompute.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				/* Betrag einlesen */
				final EditText amount_str = (EditText) findViewById(R.id.editText1);
				amount = Float.parseFloat(amount_str.getText().toString());
				/* Betrag einlesen */
				final EditText duration_str = (EditText) findViewById(R.id.editText2);
				duration = Integer.parseInt(duration_str.getText().toString());
				
				/* Zinsakkumulation ermitteln */
				boolean accumulate = true;
				final RadioGroup rg = (RadioGroup) findViewById(R.id.radioGroup1);
				switch (rg.getCheckedRadioButtonId()) {
                    case R.id.radio0:
                        accumulate = true;
                        break;
                    case R.id.radio1:
                        accumulate = false;
                        break;
                    default:
				}
				Interests in = new Interests(amount, interest, duration, accumulate); 
				float result = in.getResult();
				
				final TextView result_label = (TextView) findViewById(R.id.textView4);
				String result_str = "Kapital nach " + duration;
				if (duration == 1) 
					result_str += " Jahr: " + result;
				else 
					result_str += " Jahren: " + result;
				result_label.setText( result_str);
			}
		});
	}
}
