package de.hsos.mad;

import com.thomaskuenneth.uithreaddemo.R;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;

public class UIThreadDemo extends Activity {
	int wait_ms = 3500;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		
		final TextView textview = (TextView) findViewById(R.id.textview);
		final Button button = (Button) findViewById(R.id.button);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				textview.setText(UIThreadDemo.this.getString(R.string.begin, wait_ms));
				try {
					Thread.sleep(wait_ms);
				} catch (InterruptedException e) {
				}
				textview.setText(UIThreadDemo.this.getString(R.string.end, wait_ms));
				wait_ms += 500;
			}
		});
		final CheckBox checkbox = (CheckBox) findViewById(R.id.checkbox);
		checkbox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				textview.setText("Checkbox = " + Boolean.toString(isChecked));
			}
		});
		checkbox.setChecked(true);
	}
}