package de.hsos.mad;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity {

	private static final String TAG = MainActivity.class
			.getSimpleName();

	private static int globCounter = 1;

	private int myCounter = globCounter++;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		log("onCreate");
		setContentView(R.layout.activity_main);
		TextView tv = (TextView) findViewById(R.id.textview);
		tv.setText(getString(R.string.msg, myCounter));

		Button buttonNew = (Button) findViewById(R.id.new_activity);
		buttonNew.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(MainActivity.this,
						MainActivity.class);
				startActivity(i);
			}
		});

		Button buttonFinish = (Button) findViewById(R.id.end_activity);
		buttonFinish.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}

	@Override
	protected void onStart() {
		super.onStart();
		log("onStart");
	}

	@Override
	protected void onRestart() {
		super.onRestart();
		log("onRestart");
	}

	@Override
	protected void onResume() {
		super.onResume();
		log("onResume");
	}

	@Override
	protected void onPause() {
		super.onPause();
		log("onPause");
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		log("onDestroy");
	}

	private void log(String methodName) {		
		String msg = new String (methodName + "() #" + myCounter);
		Log.d(TAG, msg);
		if (!methodName.equals ("onCreate")) {
			EditText msg_area = (EditText) findViewById(R.id.msg_area);
			msg_area.append (msg + "\n");
		}
	}
}
