package de.hsos.ma;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.Contacts;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class CallContactActivity extends Activity implements OnClickListener {
	Toast mToast;
	int sel_id = -1;
	String sel_name = "<leer>"; 
	String sel_phone = "<leer>"; 
	
	final int cur_request_code = 4711;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Intent intent = new Intent(Intent.ACTION_PICK,
				ContactsContract.Contacts.CONTENT_URI);
		startActivityForResult(intent, cur_request_code);
		setContentView(R.layout.call_screen);
		Button b = (Button) findViewById (R.id.button1);
		b.setOnClickListener(this);
	}

	@Override
	public void onClick(View view) {
		sel_phone.trim();
		Intent i = new Intent(Intent.ACTION_DIAL,
				Uri.parse("tel:" + sel_phone));
		startActivity(i);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent rintent) {
		if (requestCode != cur_request_code)
			return; // do nothing because request code does match with returned result
		switch (resultCode) {
		case Activity.RESULT_OK:
			if (rintent != null) {
	            Uri uri = rintent.getData();
	            if (uri != null) {
	                Cursor c = null;
	                try {
	                    c = getContentResolver().query(uri, new String[] { BaseColumns._ID },
	                            null, null, null);
	                    if (c != null && c.moveToFirst()) {
	                        sel_id = c.getInt(0);
	                    }
	                    c = getContentResolver().query(uri, new String[] { Contacts.DISPLAY_NAME },
	                            null, null, null);
	                    if (c != null && c.moveToFirst()) {
	                    	sel_name = c.getString(0);
	                    }
                		String id_str = new String ("" + sel_id);
	                    c = getContentResolver().query(
	                    		Phone.CONTENT_URI, null,
	    	                    Phone.CONTACT_ID + "=?",
	    	                    new String[]{id_str}, null);	    				
	                    if(c.moveToFirst())
	    				{
	    					sel_phone = c.getString(c.getColumnIndex(Phone.DATA));
	    				}
	    				showToast (uri);
	                } finally {
	                    if (c != null) {
	                        c.close();
	                    }
	                }
	            }
	        }
			break;
		case Activity.RESULT_CANCELED:
			if (rintent != null) System.out.println(rintent.getExtras());
			// finish();
			break;
		default:
			break;
		}
	}

	public void showToast(Uri uri) {
		if (mToast != null) {
			mToast.cancel();
		}
		String txt = "Selected Phone" + ":\n" + uri;
		txt += "\nid: " + sel_id;
		txt += "\nname: " + sel_name;
		txt += "\nphone: " + sel_phone;		
		mToast = Toast.makeText(this, txt, Toast.LENGTH_LONG);
		mToast.show();
	}
}

