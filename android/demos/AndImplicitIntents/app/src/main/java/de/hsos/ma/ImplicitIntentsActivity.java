package de.hsos.ma;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ImplicitIntentsActivity extends Activity implements View.OnClickListener {
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		Button button = (Button) findViewById(R.id.button1);
		button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				// Intent i = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:++49541123456789"));
				// Man kann auch mit dem untenstehenden Aufruf direkt w�hlen;
				// dazu m�ssen aber entsprechende Permissions eingestellt sein.
				Intent i = new Intent (Intent.ACTION_DIAL, Uri.parse ("tel:(+49) 228 123456"));
				startActivity(i);
			}
		});
		button = (Button) findViewById(R.id.button2);
		button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Uri uri = Uri.parse("http://maps.google.de/maps?q=52.284634,8.023299");
				Intent i = new Intent(Intent.ACTION_VIEW);			
				// Die folgenden URIs stehen zwar so in der Google Dokumentation, 
				// die entsprechenden Intents k�nnen aber nicht aufgel�st werden.
				// Uri uri = Uri.parse("geo:52.284634,8.023299");
				// Uri uri = Uri.parse("geo:latitude,longitude");
				// Uri uri = Uri.parse("geo:latitude,longitude?z=zoom");
				// Uri uri = Uri.parse("geo:0,0?q=my+street+address");
				// Uri uri = Uri.parse("geo:0,0?q=business+near+city");
				i.setData(uri);
				startActivity(i);
			}
		});
		button = (Button) findViewById(R.id.button3);
		button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Uri uri = Uri.parse("http://maps.googleapis.com/maps/api/streetview?size=960x1640&location=52.51624,13.377195&sensor=false");
				// Die folgende URI steht zwar so in der Google Dokumentation, 
				// der entsprechende Intent kann aber nicht aufgel�st werden.
				// Uri uri = Uri.parse("google.streetview:cbll=44.640381,-63.575911");
				Intent i = new Intent(Intent.ACTION_VIEW, uri);
				startActivity(i);
			}
		});
		button = (Button) findViewById(R.id.button4);
		button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Uri uri = Uri.parse("http://www.hs-osnabrueck.de");
				// Intent i = new Intent(Intent.ACTION_WEB_SEARCH, uri);
				Intent i = new Intent(Intent.ACTION_VIEW, uri);
				startActivity(i);
			}
		});
		
		button = (Button) findViewById(R.id.button5);
		button.setOnClickListener(this);
		
		button = (Button) findViewById(R.id.button6);
		button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setType("vnd.android-dir/mms-sms");
				intent.putExtra("address", "5554");
				intent.putExtra("sms_body", "Eine Nachricht!");
				startActivity(intent);
			}
		});
	}
	
	public void onClick (View v) {
		/*
		Intent i = new Intent(Intent.ACTION_PICK, 
				ContactsContract.Contacts.CONTENT_URI);
		*/
		/* Deprecated */
		/*
		Intent i = new Intent(Intent.ACTION_PICK, 
				People.CONTENT_URI);
		*/
		// startActivityForResult(i, 1);
		final Intent i = new Intent (this, CallContactActivity.class);
		startActivity(i);
	}	
}