package de.hsos.mad;

import android.os.Bundle;
import android.app.Activity;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MessageActivity extends Activity {

	private static final String ACTION_BROADCAST_DEMO = "de.hsos.mad.action.BROADCAST_DEMO";
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_message);
		Button button;
		button = (Button) findViewById(R.id.btnBroadcast);
		button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				final Intent bI = new Intent(ACTION_BROADCAST_DEMO);
				getApplicationContext().sendBroadcast(bI);
			}
		});
		button = (Button) findViewById(R.id.btnNotification);
		button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				notifyUser("21", "Osnabr�ck Hbf", "09:24", 10);
			}
		});		
	}

	// Code f�r Dynamischen Broadcast Receiver
	private MyBroadcastReceiver myBroadcastReceiver;

	private class MyBroadcastReceiver extends BroadcastReceiver {
		public void onReceive(Context ctxt, Intent intent) {
			String action = intent.getAction();
			Toast.makeText(MessageActivity.this,
					"Broadcast eingegangen: " + action, Toast.LENGTH_SHORT).show();
		}
	}

	// Bei onResume() Broadcast Receiver registrieren.
	@Override
	public void onResume() {
		super.onResume();
		IntentFilter iF = new IntentFilter(ACTION_BROADCAST_DEMO);
		myBroadcastReceiver = new MyBroadcastReceiver();
		getApplicationContext().registerReceiver(myBroadcastReceiver, iF);
	}

	// Bei onPause() Broadcast Receiver de-registrieren.
	@Override
	public void onPause() {
		super.onPause();
		getApplicationContext().unregisterReceiver(myBroadcastReceiver);
	}
	
	// ID der Nofifikation
	private static int NOTIFICATION_ID = 0;

	private void notifyUser(String busLine, String busStop, String departure,
			int minDelay) {
		String text = "Bus " + busLine + " (Abfahrt " + departure + " ist um "
				+ minDelay + " Minuten versp�tet.";

		final Context ctxt = getApplicationContext();
		Notification notif = new Notification(R.drawable.ic_launcher, text, System.currentTimeMillis());
		
		//
		// Bei Android 3.0 wurde der Notification Builder eingef�hrt, 
		// der aber hier nichts vereinfacht und bei Android < 3.0 
		// zu einer NoClassDefFoundError Exception f�hrt, wenn man 
		// nicht das Compatibility Package installiert hat.
		//
		//		Notification notif = new Notification.Builder(ctxt)
		//				.setContentTitle("Busversp�tung").setContentText(text)
		//				.setSmallIcon(R.drawable.ic_launcher).build();

		final NotificationManager nm;
		nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

		// Dieser Intent wird der Notifikation hinterlegt und 
		// kann beim �ffnen des Intents gestartet werden. 
		// Hier soll eine Aktiviti�t aufgerufen werden.
		Intent notifIntent = new Intent(ctxt, DisplayActivity.class);
		notifIntent.putExtra("BUS_LINE", busLine);
		notifIntent.putExtra("DEPARTURE", departure);
		notifIntent.putExtra("BUS_STOP", busStop);
		notifIntent.putExtra("MIN_DELAY", minDelay);
		notifIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

		PendingIntent startIntent;
		startIntent = PendingIntent.getActivity(ctxt, 0, notifIntent, 0);
		notif.setLatestEventInfo(ctxt, "Busfahrplan ansehen", "Buslinie: "
				+ busLine + ", Haltestelle: " + busStop, startIntent);
		notif.vibrate = new long[] { 100, 150 };
		nm.notify(NOTIFICATION_ID++, notif);
	}
}