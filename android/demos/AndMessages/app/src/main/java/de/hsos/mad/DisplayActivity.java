package de.hsos.mad;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

public class DisplayActivity extends Activity {
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_display);
		
		final Intent i = getIntent();

		final String busStop = i.getStringExtra("BUS_STOP");
		final String departure = i.getStringExtra("DEPARTURE");

		final int notificationNr = i.getIntExtra("notificationNr", 0);

		final NotificationManager nm;
		nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		nm.cancel(notificationNr);

		showBusSchedule (this, busStop, departure);
	}
	
	static private int getBusStopCode(String busStop) {
		if (busStop == null)
			return 0;
		if (busStop.equals("Aachen Hbf"))
			return 8000001;
		// ...
		if (busStop.equals("Osnabrueck Hbf"))
			return 8000294;
		if (busStop.equals("Osnabr�ck Hbf"))
			return 8000294;
		if (busStop.equals("Osterburken"))
			return 8000295;
		if (busStop.equals("Paderborn Hbf"))
			return 8000297;
		// ...
		return 0;
	}
		
	static public void showBusSchedule (Context ctxt, String busStop, String departure) {
		String uri_strg = "http://www.fahrplaner.de/hafas/stboard.exe/dn?ld=web&input="
				+ Integer.valueOf (getBusStopCode(busStop)).toString() // Code 8000294 f�r 'Osnabr�ck Hbf'
				+ "&productsFilter=0&time=" + departure
				+ "&disableEquivs=yes&maxJourneys=20&&showResultPopup=popup&start=1";
		
		Uri uri = Uri.parse(uri_strg);
		Intent viewIntent = new Intent(Intent.ACTION_VIEW, uri);
		// ben�tigt f�r SMS Receiver
		viewIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		ctxt.startActivity(viewIntent);
	}
}
