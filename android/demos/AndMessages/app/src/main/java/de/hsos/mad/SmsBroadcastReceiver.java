package de.hsos.mad;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.widget.Toast;

public class SmsBroadcastReceiver extends BroadcastReceiver {
	private static final String SMS_DATA_ACTION = "android.intent.action.SMS_DATA_ACTION";
	private static final short SMS_DATA_PORT = 16984;

	public void onReceive(Context ctxt, Intent intent) {
		// Achtung: dies funktioniert im Emulator aufgrund eines Fehlers dort nicht. 
		// Wird eine Daten-SMS gesendet, wird auf der Empf�nger-Seite
		// ein Intent erzeugt mit der Action SMS_RECEIVED, SMS_DATA_ACTION ist
		// nicht gesetzt, d.h. die eingehende SMS wird als Txt-SMS behandelt,
		// was zu fehlerhaftem Verhalten f�hrt.
		if (intent.getAction().equals(SMS_DATA_ACTION)) {
			Toast.makeText(ctxt, "Keine Behandlung von Daten-SMS im Emulator!", Toast.LENGTH_SHORT).show();
			processDataSMS(ctxt, intent);
		} else {
			processTextSMS(ctxt, intent);
		}
	}

	// Verarbeitung einer Daten SMS.
	private void processDataSMS(Context ctxt, Intent i) {
		final String uricontent = i.getDataString();
		final String[] str = uricontent.split(":");
		final String strPort = str[str.length - 1];
		final int port = Integer.parseInt(strPort);
		
		if (port == SMS_DATA_PORT) {
			final Bundle data = i.getExtras();
			if (data != null) {
				final Object[] pduObj = (Object[]) data.get("pdus");
				for (Object pdu : pduObj) {
					SmsMessage sms = SmsMessage.createFromPdu((byte[]) pdu);
					sms = SmsMessage.createFromPdu((byte[]) pdu);
					if (sms != null) {
						String smsTxt = new String(sms.getUserData());
						if (smsTxt != null) {
							String senderNo = sms.getOriginatingAddress();
							if (showBusSchedule(ctxt, senderNo, smsTxt) == false)
								Toast.makeText(ctxt, "Fehler in Daten-SMS: " + smsTxt, Toast.LENGTH_SHORT).show();
						}
					}
				}
			}
		}
	}

	// Verarbeitung einer Text SMS.
	private void processTextSMS(Context ctxt, Intent intent) {
		final Bundle data = intent.getExtras();
		if (data != null) {
			final Object[] pduUnits = (Object[]) data.get("pdus");
			for (Object pdu : pduUnits) {
				SmsMessage sms = SmsMessage.createFromPdu((byte[]) pdu);
				sms = SmsMessage.createFromPdu((byte[]) pdu);
				if (sms != null) {
					String smsTxt = new String(sms.getMessageBody());
					if (smsTxt != null) {
						String senderNo = sms.getOriginatingAddress();
						if (showBusSchedule(ctxt, senderNo, smsTxt) == false)
							Toast.makeText(ctxt, "Fehler in Text-SMS: " + smsTxt, Toast.LENGTH_LONG).show();
					}
				}
			}
		}		
	}

	// Reaktion auf SMS-Eingang: Anzeige des Bus-Fahrplans.
	private boolean showBusSchedule(Context ctxt, String senderNo, String smsTxt) {
		// <sender no>#<topic>#<bus stop>#<departure>
		// Beispiel: smsTxt = "++491735223001#Busverspaetung#Osnabrueck Hbf#9:24";
		String[] token = smsTxt.split("#");
		if (token == null) return false;

		String busStop; 
		String departure;
		try {
			busStop = token[2];
			departure = token[3];
		}
		catch (java.lang.ArrayIndexOutOfBoundsException e) {
			return false;
		}
		  
		if ((busStop == null) || (departure == null))
			return false;

		de.hsos.mad.DisplayActivity.showBusSchedule (ctxt, busStop, departure);
		return true;
	}
}